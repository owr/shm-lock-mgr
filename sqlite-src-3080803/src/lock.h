#ifndef __LOCK_H__
#define __LOCK_H__
#include "./page_lock.h"

#define META_PAGE 50
#define M_META_PAGE -50

static int is_meta_page(Pgno pgno)
{
	if (pgno == 170165 || pgno == 170166)
		return true;
	return pgno <= META_PAGE;
}

static int lock_page(TXNId txn_id, Pgno pgno)
{
	if (is_meta_page(pgno))
		return M_META_PAGE;
	int ret = txn_lock_acquire(txn_id, pgno);
	if (ret)
		return SQLITE_LOCKED;
	return ret;
}

#endif
