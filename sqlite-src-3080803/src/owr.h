#ifndef __OWR_H__
#define __OWR_H__

/*
 *#define DEBUG 1
 */
/*
 *#define SQLITE_DEBUG 1
 *#define VDBE_PROFILE 1
 */
//#define SQLITE_TEST 1
/*
 *#define SQLITE_ENABLE_IOTRACE 1
 *#define SQLITE_DEBUG 1
 */

#ifdef DEBUG
#define dbg_print(x...) do {\
	printf("%s,%s,%d: ", __FILE__, __func__, __LINE__); \
	printf(x); \
} while (0)

#define measure_time(stmt, delta) do { \
	uint64_t start_nsec, end_nsec; \
	struct timespec ts;	       \
	getnstimeofday(&ts);           \
	start_nsec = timespec_to_ns(&ts); \
	stmt;			       \
	getnstimeofday(&ts);           \
	end_nsec = timespec_to_ns(&ts); \
	delta = end_nsec - start_nsec;  \
} while (0)

#else

#define dbg_print(x...) do { } while (0)

#define measure_time(stmt, delta) do { \
	stmt;			       \
} while (0)

#endif

#endif
