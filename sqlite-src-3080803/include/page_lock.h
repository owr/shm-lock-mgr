#ifndef __PAGE_LOCK_H__
#define __PAGE_LOCK_H__

#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/syscall.h>

/*
 *#define MTX_NOLOCK 1
 */
/*
 *#define PGLK_DEBUG
 *#define PGLK_DEBUG1
 */
/*
 *#define DEADLOCK_DEBUG
 */

/*
 * We can use mutex or rwlock to lock the lockmanager. But seqlock
 * can be more efficient than mutex. As c++ container library is
 * MRSW safe, we can use seqlock to lock the pointer of the container
 * to guarentee thread safe
 */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * errno
 */
#define DDLOCK 3

#define NO_LOCK 0
#define RD_LOCK 1
#define WR_LOCK 2
#define TO_RECYCLE 3
#define SHARED_RWLOCK "owr_SharedRWLock"
#define HASH_MAP_NAME "owr_SharedHashMap"
#define SHARED_TXN_LOCKMAP "owr_SharedTXN_LockMap"
#define SHARED_DGRAPH_LOCK "owr_Shared_DGraph_Lock"
#define SHARED_DGRAPH "owr_Shared_DGraph"
#define SHARED_DGRAPH_R "owr_Shared_DGraph_R"


#define gettid() syscall(SYS_gettid)
#define DUMMY_PID 12345678

/**
 * our lock manager, make it memory shared
 */
extern void *gbl_lock_mgr;

/**
 * init the whole stuff, every process need to call this to
 * init the above two global variables
 */
extern void lock_mgr_init();

/**
 * destroy the resources allocated, should by called by the
 * last process, since we don't know the which one is the last,
 * we can keep it in memory forever, since it's shared by all
 * processes/threads
 */
extern void lock_mgr_destroy();

/**
 *
 * return the lock status
 * 0, succed
 * 1, not exist
 * -1 failed
 */
extern int mgr_rpage_lock(uint32_t pgno);
extern int mgr_rpage_unlock(uint32_t pgno);

extern int mgr_wpage_lock(uint32_t pgno);
extern int mgr_wpage_unlock(uint32_t pgno);
extern int mgr_insert_page(uint32_t pgno);
extern struct PageLockMtx *mgr_search_page(uint32_t pgno);
extern int mgr_enter_mutex(uint32_t pgno);
extern int mgr_try_enter_mutex(uint32_t pgno);
int mgr_mutex_keep_trying(uint32_t pgno, uint64_t l);
extern int mgr_leave_mutex(uint32_t pgno);

extern int mgr_leave_mutex_all(uint32_t pgno);
/**
 * delete the seg var
 */
void lock_mgr_destroy_segvar();

extern void dump_mrg();

/**
 * for test
 */
//ShmRWLock* init_shared_rwlock(const char* lock_name);
/*
 *void destroy_shared_rwlock(const char *lock_name);
 */
/*
 *void do_read_page(uint32_t pgno, int sleep_time, int usleep_time);
 *void do_write_page(uint32_t pgno, int sleep_time, int usleep_time);
 */

/**
 * for debug
 */
extern int64_t gbl_txn_id;

/********************* for transaction lock list ******************/
/**
 * Manipulate locks holding on transactions
 */
typedef uint32_t TXNId;
typedef uint32_t Pgno;

extern int txn_lock_acquire(TXNId txn_id, Pgno pgno);
extern int txn_lock_try_acquire(TXNId txn_id, Pgno pgno);
extern int txn_lock_release(TXNId txn_id, Pgno pgno);
extern int txn_lock_release_all(TXNId txn_id);

extern int add_lock_to_txn(TXNId txn_id, Pgno pgno);
extern bool is_lock_held(TXNId txn_id, Pgno pgno);
extern void dump_locks_on_txn(TXNId txn_id);

/*** helper functions ***/
static int64_t int_concat(int64_t a, int64_t b)
{
	char str[128];
	sprintf(str, "%ld%ld", a, b);
	return atol(str);
}


#ifdef __cplusplus
}
#endif

#endif
