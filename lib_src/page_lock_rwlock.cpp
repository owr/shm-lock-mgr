#include <pthread.h>
#include <atomic>
#include <vector>
#include <thread>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <sys/wait.h>
#include "shm-map.hpp"
#include "page_lock.h"
#include "common.h"
#include <mutex>
using namespace std;
using namespace boost::interprocess;


#ifdef __cplusplus
extern "C" {
#endif

struct ShmSegLock *gbl_seg_lck;
void *gbl_lock_mgr = nullptr;
std::shared_ptr<managed_shared_memory> gbl_mem_seg;

static struct PageLock *_mgr_search_page(uint32_t pgno);
static int _mgr_insert_page(uint32_t pgno);

/**
 * first, we need a **shared** lock to protect the big shared lock manager
 * then we create the shared lock manager
 */
ShmSegLock *init_shared_seglock(const char* lock_name)
{
	auto _seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);

	assert(_seg);
	auto pr = find_shm_obj<ShmSegLock>(_seg, lock_name);
	ShmSegLock *seglock = nullptr;

	if (!pr.first) {
		seglock = _seg->construct<ShmSegLock>(lock_name)();

		pthread_rwlockattr_t rwlock_attr;
		pthread_rwlockattr_setpshared(&rwlock_attr,
									  PTHREAD_PROCESS_SHARED);
		/**
		 * write has higher priority
		 */
		pthread_rwlockattr_setkind_np(&rwlock_attr,
			  PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);

		pthread_rwlock_init(&seglock->seg_lock, &rwlock_attr);
		seglock->ref_count = 0;
#ifdef PGLK_DEBUG
		std::cout << "tid: " << gettid() << ", created rwlock: " << lock_name << std::endl;
#endif

	} else {
#ifdef PGLK_DEBUG
		std::cout << "tid: " << gettid() << ", rwlock: " << lock_name
			<< " exists, just return" << std::endl;
#endif
		seglock = pr.first;
	}
	gbl_mem_seg = _seg;
	return seglock;
}

/**
 * static
 */
void destroy_shared_seglock(const char *lock_name)
{
	std::cout << lock_name << std::endl;
	destroy_shm_object<ShmSegLock>(gbl_mem_seg, lock_name);
}

void lock_mgr_init()
{

	gbl_seg_lck = init_shared_seglock(SHARED_RWLOCK);
	int32_t ref_cnt = gbl_seg_lck->ref_count.fetch_add(1);
	std::cout << "tid: " << gettid() << ", ref_cnt: " << ref_cnt << endl;
	//std::cout << "tyname: " << typeid(ref_cnt).name() << std::endl;

	pthread_rwlock_wrlock(&gbl_seg_lck->seg_lock);
	gbl_lock_mgr = create_shm_hashmap(HASH_MAP_NAME);
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);
}

void lock_mgr_destroy()
{
#if 0
	int32_t ref_cnt = gbl_seg_lck->ref_count.fetch_sub(1);
	std::cout << "tid: " << gettid() << ", ref_cnt: " << ref_cnt << endl;
	if (ref_cnt == 0) {
#endif
#if 1
		pthread_rwlock_destroy(&gbl_seg_lck->seg_lock);
		// destroy SHARED_RWLOCK in shared memory segment
		destroy_shared_seglock(SHARED_RWLOCK);
		// destroy shared hashmap in shared memory segment
		destroy_shm_hashmap(HASH_MAP_NAME);
		// destroy the shared segment itself
		destroy_seg();
#endif
#if 0
	}
#endif
}

void lock_mgr_destroy_segvar()
{
	//delete static_cast<managed_shared_memory*>(gbl_mem_seg);
	gbl_mem_seg = nullptr;
}


/**
 * return the lock status
 * 0, succed
 * 1, not exist
 * -1 failed
 */
int mgr_rpage_lock(uint32_t pgno)
{
#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", mgr_rpage_lock: " << pgno << endl;
#endif

	struct PageLock *_pglck = nullptr;

	/**
	 * lock the lock manager to search a page, manager read
	 */
	pthread_rwlock_wrlock(&gbl_seg_lck->seg_lock);
	_pglck = _mgr_search_page(pgno);

#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", before _pglock: " << _pglck << endl;
#endif
	if (_pglck == nullptr) {
//#ifdef PGLK_DEBUG
		std::cout << "tid: " << gettid() << ", rpage insert pgno: " << pgno << endl;
//#endif
		_mgr_insert_page(pgno);
	}
	_pglck = _mgr_search_page(pgno);
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);

#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", after _pglock: " << _pglck << endl;
#endif
	assert(_pglck != nullptr);
	auto ret = pthread_rwlock_rdlock(&_pglck->pg_rwlock);
	_pglck->stat = RD_LOCK;

#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", ret: " << ret << endl;
#endif

	return ret;
}

int mgr_rpage_unlock(uint32_t pgno)
{
#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", mgr_rpage_unlock: " << pgno << endl;
#endif
	struct PageLock *_pglck;

	pthread_rwlock_rdlock(&gbl_seg_lck->seg_lock);
	_pglck = _mgr_search_page(pgno);
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);

	assert(_pglck != nullptr);
	_pglck->stat = NO_LOCK;
	return pthread_rwlock_unlock(&_pglck->pg_rwlock);
}

int mgr_wpage_lock(uint32_t pgno)
{
#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", mgr_wpage_lock: " << pgno << endl;
#endif
	struct PageLock *_pglck = nullptr;

	// should use write lock here, because we can potentially insert into hashtable
	pthread_rwlock_wrlock(&gbl_seg_lck->seg_lock);
	_pglck = _mgr_search_page(pgno);

#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", before _pglock: " << _pglck << endl;
#endif
	if (_pglck == nullptr) {
#ifdef PGLK_DEBUG
		std::cout << "tid: " << gettid() << ", wpage insert pgno: " << pgno << endl;
#endif
		_mgr_insert_page(pgno);
	}
	_pglck = _mgr_search_page(pgno);
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);

#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", after _pglock: " << _pglck << endl;
#endif

	assert(_pglck != nullptr);
	/**
	 * request a write lock
	 */
	auto ret = pthread_rwlock_wrlock(&_pglck->pg_rwlock);
	std::cout << "tid: " << gettid() << ", ret: " << ret << endl;
	std::cout << "tid: " << gettid() << ", got wlock " << ret << endl;
	_pglck->stat = WR_LOCK;

	return ret;
}

int mgr_wpage_unlock(uint32_t pgno)
{
#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", mgr_wpage_unlock: " << pgno << endl;
#endif
	struct PageLock *_pglck;

	pthread_rwlock_rdlock(&gbl_seg_lck->seg_lock);
	_pglck = _mgr_search_page(pgno);
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);

	assert(_pglck != nullptr);
	_pglck->stat = NO_LOCK;
	return pthread_rwlock_unlock(&_pglck->pg_rwlock);
}

static int _mgr_insert_page(uint32_t pgno)
{
	PageLock _lock;
	memset(&_lock, 0, sizeof(PageLock));
	_lock.pageno = pgno;
	_lock.stat = NO_LOCK;

	pthread_rwlockattr_t rwlock_attr;
	pthread_rwlockattr_init(&rwlock_attr);
	/**
	 * set process shared, important
	 */
	pthread_rwlockattr_setpshared(&rwlock_attr,
						  PTHREAD_PROCESS_SHARED);
	/**
	 * write has higher priority
	 */
	pthread_rwlockattr_setkind_np(&rwlock_attr,
			  PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);


	/**
	 * check the page lock if already exists, if so, just return
	 * this could happen when there are two writers trying to insert
	 * the same page
	 */
	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	if (exists)
		goto ret;
	/**
	 * insert the lock
	 */
	obj.insert(ValueType(pgno, _lock));
	pthread_rwlock_init(&obj[pgno].pg_rwlock, &rwlock_attr);
ret:

	return 0;
}

int mgr_insert_page(uint32_t pgno)
{
	PageLock _lock;
	memset(&_lock, 0, sizeof(PageLock));
	_lock.pageno = pgno;
	_lock.stat = NO_LOCK;

	pthread_rwlockattr_t rwlock_attr;
	pthread_rwlockattr_init(&rwlock_attr);
	/**
	 * set process shared, important
	 */
	pthread_rwlockattr_setpshared(&rwlock_attr,
						  PTHREAD_PROCESS_SHARED);
	/**
	 * write has higher priority
	 */
    /*
	 *pthread_rwlockattr_setkind_np(&rwlock_attr,
	 *          PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);
     */
	pthread_rwlockattr_setkind_np(&rwlock_attr,
			  PTHREAD_RWLOCK_PREFER_WRITER_NRECURSIVE_NP);


	/**
	 * lock the lock manager to insert a page, manager write
	 */
	pthread_rwlock_wrlock(&gbl_seg_lck->seg_lock);
	/**
	 * check the page lock if already exists, if so, just return
	 * this could happen when there are two writers trying to insert
	 * the same page
	 */

	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	if (exists)
		goto ret;
	/**
	 * insert the lock
	 */
	obj.insert(ValueType(pgno, _lock));
	pthread_rwlock_init(&obj[pgno].pg_rwlock, &rwlock_attr);
ret:
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);

	return 0;
}

static struct PageLock *_mgr_search_page(uint32_t pgno)
{
	struct PageLock *_pglck = nullptr;

	/**
	 * lock the lock manager to search a page, manager read
	 */
	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	std::cout << "tid: " << gettid() << ", count: " << exists << endl;
	if (exists)
		_pglck = &obj[pgno];

	return _pglck;
}


struct PageLock *mgr_search_page(uint32_t pgno)
{
	struct PageLock *_pglck = nullptr;

	/**
	 * lock the lock manager to search a page, manager read
	 */
	pthread_rwlock_rdlock(&gbl_seg_lck->seg_lock);
	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	if (exists)
		_pglck = &obj[pgno];

	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);
	return _pglck;
}

void lock_mgr_recycle_page(uint32_t pgno)
{

}

/**
 * for test
 */
void dump_mrg()
{
	pthread_rwlock_wrlock(&gbl_seg_lck->seg_lock);
	for (auto &x : *static_cast<UODR_HashMap*>(gbl_lock_mgr))
		std::cout << "pgno: " << x.first << ", "
			<< x.second.pageno << ", lock: "
			<< &x.second.pg_rwlock << std::endl;
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);
}

/**
 * static
 */
void do_read_page(uint32_t pgno, int sleep_time, int usleep_time)
{
	std::string s = usleep_time == 0 ? "child" : "parent";
	usleep(usleep_time);

	std::cout << s << " try to get lock on page " << pgno << std::endl;
	mgr_rpage_lock(pgno);
	std::cout << s << " got lock on page " << pgno << std::endl;

	std::cout << s << " sleep 1 sec" << std::endl;
	sleep(sleep_time);

	mgr_rpage_unlock(pgno);
	std::cout << s << " released lock on page " << pgno << std::endl;
}

/**
 * static
 */
void do_write_page(uint32_t pgno, int sleep_time, int usleep_time)
{
	std::string s = usleep_time == 0 ? "child" : "parent";

	usleep(usleep_time);
	std::cout << s << " try to get lock on page " << pgno << std::endl;
	mgr_wpage_lock(pgno);
	sleep(sleep_time);

	std::cout << s << " got lock on page " << pgno << std::endl;
	mgr_wpage_unlock(1);
	std::cout << s << " release lock on page " << pgno << std::endl;
}

#ifdef __cplusplus
}
#endif
