#ifndef _SEQ_LOCK_HPP_
#define _SEQ_LOCK_HPP_

#include <atomic>
using namespace std;


/*
 *static atomic<uint64_t> seq;
 *static atomic<DATA_TYPE> data;
 */
#define WRITE_BEGIN(seq, seq0) \
	seq0 = seq; \
	do { \
		while (seq0 & 1 || \
			   !seq.compare_exchange_weak(seq0, seq0+1)) ; \
	} while (0);

#define WRITE_END(seq) \
	do { \
		seq = seq0 + 2; \
	} while (0);


#define READ_BEGIN(seq, seq0, seq1, TYPE, holder, data) \
	TYPE holder; \
	do { \
		seq0 = seq.load(memory_order_acquire); \
		holder = data.load(memory_order_relaxed); \
		atomic_thread_fence(memory_order_acquire); \
		seq1 = seq.load(memory_order_relaxed); \
	} while (seq0 != seq1 || seq0 & 1);

#define READ_END()


#if 0
static void write_data(DATA_TYPE val)
{
	uint64_t seq0 = seq;
	while (seq0 & 1 ||
		   !seq.compare_exchange_weak(seq0, seq0+1)) ;

	data = val;

	seq = seq0 + 2;
}

DATA_TYPE read_data()
{
	DATA_TYPE r;
	uint64_t seq0, seq1;
	do {
		seq0 = seq;
		r = data;
		seq1 = seq;
	} while (seq0 != seq1 || seq0 & 1);

	cout << r << endl;
	return r;
	// do with r1
}

static DATA_TYPE read_data()
{
	DATA_TYPE r;
	uint64_t seq0, seq1;
	do {
		seq0 = seq.load(memory_order_acquire);
		r = data.load(memory_order_relaxed);
		atomic_thread_fence(memory_order_acquire);
		seq1 = seq.load(memory_order_relaxed);
	} while (seq0 != seq1 || seq0 & 1);

	return r;
}
#endif

#if 0
int main(int argc, char *argv[])
{
	write_data(3);
	cout << data << endl;
	read_data();
	return 0;
}
#endif

#endif
