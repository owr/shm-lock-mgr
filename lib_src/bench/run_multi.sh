scales="
1000
2000
3000
4000
5000
10000
20000
40000
80000
160000
320000
640000
1280000
2560000
5120000"

types="
1
2
3
0"

for s in $scales
do
	echo > ./res_dir/res_${s}
	for i in `seq 1000`
	do
		./benchmark $i $s $t | awk -v i="$i" -F'[: ]' 'BEGIN{V=0}{V+=$4}END{print "agv: ", i, V/NR}' >> ./res_dir/res_${s}
	done
done

