#!/usr/bin/python
import re
import time
import fileinput


#3 1000-bench_tp-3-  1239281.320000

def cal_agv():
	th_nr = 0 
	cost_sum_r = 0.0 
	cost_sum_w = 0.0 
	print "#th_nr, rlock_latency, wlock_latency"
	for l in fileinput.input():
		l = l.strip()
		match = re.search("TIME", l)
		if len(l) == 0:
			continue
		if match != None:
			if th_nr == 0:
				continue
			#print th_nr, "\t", cost_sum_r/th_nr, "\t", cost_sum_w/th_nr 
			print th_nr, ',', cost_sum_r/th_nr, ',' ,cost_sum_w/th_nr 
			continue 
		_l = l.strip().split()
		th_nr = int(_l[0])
		rw = int(_l[1].split('-')[2])
		if rw == 2: 
			cost_sum_r = cost_sum_r + float(_l[2])
		elif rw == 3:
			cost_sum_w = cost_sum_w + float(_l[2])
		else:
			print "error"

	print th_nr, ',', cost_sum_r/th_nr, ',' ,cost_sum_w/th_nr 

if __name__ == "__main__":
	cal_agv()
