# Makefile.inc - common definitions used by all makefiles

SHMLK_DIR = ../
SHMLK_LIB = ${SHMLK_DIR}/lib/libapi.so
SHMLK_INCL_DIR = ${SHMLK_DIR}/include/

LINUX_LIBRT = -lrt
LINUX_LIBDL = -ldl
LINUX_LIBACL = -lacl
LINUX_LIBCRYPT = -lcrypt
LINUX_LIBCAP = -lcap
#LINUX_LIBGTEST = -lgtest
LINUX_LIBBOOST = -lboost_system -lboost_thread

CXX=clang++
CC=clang
IMPL_CFLAGS = -I${SHMLK_INCL_DIR} \
			  -pedantic \
			  -W \
			  -Wno-sign-compare \
			  -Wno-unused-parameter -g

CFLAGS = ${IMPL_CFLAGS}
CXXFLAGS = -std=c++1y ${IMPL_CFLAGS}
CCFLAGS = ${IMPL_CFLAGS}
IMPL_THREAD_FLAGS = -lpthread
IMPL_LDLIBS = ${SHMLK_LIB} -lm
LDLIBS = ${IMPL_LDLIBS}
RM = rm -f

LINK_FLAGS = ${LDLIBS} ${IMPL_THREAD_FLAGS} ${LINUX_LIBBOOST} ${LINUX_LIBGTEST} ${LINUX_LIBRT}
#LINK_FLAGS = ${IMPL_THREAD_FLAGS} ${LINUX_LIBBOOST} ${LINUX_LIBGTEST} ${LINUX_LIBRT}

