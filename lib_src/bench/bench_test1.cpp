#include <iostream>
#include <chrono>
#include <vector>
#include <thread>
#include <unistd.h>
#include <stdlib.h>
#include <mutex>

using namespace std;
using namespace std::chrono;
typedef high_resolution_clock::time_point Time;
typedef duration<double, nano> Span;
mutex mtx;

void thread_entity()
{
	Time t1, t2;
	Span time_span;
	vector<int> v;
	long acc_time = 0;
	for (int i  = 0; i < 1000000; ++i) {
		t1 = high_resolution_clock::now();
		v.push_back(i);
		t2 = high_resolution_clock::now();
		time_span = duration_cast<Span>(t2 - t1);
		acc_time += time_span.count();
	}
	mtx.lock();
	//cout << time_span.count() << " nanoseconds" << endl;
	cout << acc_time << " nanoseconds" << endl;
	mtx.unlock();
}

void thread_entity2()
{
	Time t1, t2;
	Span time_span;
	vector<int> v;
	long acc_time = 0;
	t1 = high_resolution_clock::now();
	for (int i  = 0; i < 1000000; ++i) {
		v.push_back(i);
	}
	t2 = high_resolution_clock::now();
	time_span = duration_cast<Span>(t2 - t1);
	acc_time += time_span.count();
	mtx.lock();
	//cout << time_span.count() << " nanoseconds" << endl;
	cout << acc_time << " nanoseconds" << endl;
	mtx.unlock();
}


#define N 1000
int main(int argc, char *argv[])
{
	vector<thread> v(N);
	for (int i = 0; i < N; ++i) {
		v[i] = thread(thread_entity2);
	}

	for (int i = 0; i < N; ++i)
		v[i].join();

	return 0;
}

