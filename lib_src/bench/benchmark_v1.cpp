#include "api.h"
#include <string.h>
#include <assert.h>
#include <wait.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ratio>
#include <map>
#include <vector>
#include <iostream>
#include <chrono>
using namespace std;
using namespace std::chrono;

typedef high_resolution_clock::time_point Time;
typedef duration<double, nano> Span;
typedef int(*func)(uint32_t);

#define BENCH_SIZE 30


vector<int> scale;
vector<int> init_rand_nums_v;
map<int, bool> init_rand_nums;
map<int, bool> bench_rand_nums;

static void init_scale()
{
	scale.clear();
	// first case is for warming up
	int i = 5;
	for (int j = 0; j < 7; ++j) {
		vector<int> v;
		scale.push_back(i);
		i *= 10;
	}
}

static void get_rand_vec(vector<int> &v)
{
	//srand(time(0));
	while (v.size() < BENCH_SIZE)
		v.push_back(init_rand_nums_v[rand() % init_rand_nums_v.size()]);
}

static void init()
{
	init_scale();
}

static void insert_bench(Time &t1, Time &t2, vector<int> &v)
{
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SIZE; ++i)
		mgr_insert_page(v[i]);
	t2 = high_resolution_clock::now();
#if 0
	cout << __func__ << endl;
	for (auto x : v)
		cout << x << endl;
#endif
}

static void search_bench(Time &t1, Time &t2)
{
	vector<int> _v;
	//srand(113);
	get_rand_vec(_v);
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SIZE; ++i)
		mgr_insert_page(_v[i]);
	t2 = high_resolution_clock::now();
#if 0
	cout << __func__ << endl;
	for (auto x : _v)
		cout << x << endl;
#endif
}

static void rlock_bench(Time &t1, Time &t2)
{
	int j = 0;
	vector<int> _v;
	//srand(13);
	get_rand_vec(_v);
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SIZE; ++i) {
		mgr_rpage_lock(_v[i]);
		mgr_rpage_unlock(_v[i]);
	}
	t2 = high_resolution_clock::now();
#if 0
	cout << __func__ << endl;
	for (auto x : _v)
		cout << x << endl;
#endif
}

static void wlock_bench(Time &t1, Time &t2)
{
	vector<int> _v;
	//srand(11);
	get_rand_vec(_v);
	t1 = high_resolution_clock::now();
	int j = 0;
	for (int i = 0; i < BENCH_SIZE; ++i) {
		mgr_wpage_lock(_v[i]);
		j++;
		mgr_wpage_unlock(_v[i]);
	}
	t2 = high_resolution_clock::now();
#if 0
	cout << __func__ << endl;
	for (auto x : _v)
		cout << x << endl;
#endif
}

void bench_entity(int _scale, int _bench_type)
{
	Time t1, t2;
	Span time_span;
	vector<int> v(BENCH_SIZE, 0);
	int i = 0;
	assert(bench_rand_nums.size() == BENCH_SIZE);

	for (auto &x : bench_rand_nums) {
		v[i++] = x.first;
	}

	string _bench_name;
	switch (_bench_type) {
	case 0:
		insert_bench(t1, t2, v);
		_bench_name = "insert";
		break;
	case 1:
		search_bench(t1, t2);
		_bench_name = "search";
		break;
	case 2:
		//sleep(1);
		rlock_bench(t1, t2);
		_bench_name = "rlock";
		break;
	case 3:
		//sleep(1);
		wlock_bench(t1, t2);
		_bench_name = "wlock";
		break;
	default:
		break;
	}
	time_span = duration_cast<Span>(t2 - t1);
	if (_scale > 5)
		std::cout << _bench_name << "- " << _scale << ": " <<
			time_span.count()/BENCH_SIZE << " nanoseconds." << endl;
}

static void single_round(int _scale)
{
	srand(31);
	init_rand_nums.clear();
	init_rand_nums_v.clear();
	while (init_rand_nums.size() < _scale) {
		int rd = rand() % INT_MAX;
		init_rand_nums[rd] = rd;
	}

	//srand(51);
	//sleep(1);
	while (bench_rand_nums.size() < BENCH_SIZE) {
		int rd = rand() % INT_MAX;
		if (!init_rand_nums[rd])
			bench_rand_nums[rd] = rd;
	}

	for (auto &x : init_rand_nums) {
		init_rand_nums_v.push_back(x.first);
	}
	for (int i = 0; i < _scale; ++i)
		mgr_insert_page(init_rand_nums_v[i]);

	bench_entity(_scale, 0);
	bench_entity(_scale, 2);
	bench_entity(_scale, 1);
	bench_entity(_scale, 3);
}

void insert_locate_bench() {
	lock_mgr_destroy();
	lock_mgr_init();
	init();

	assert(gbl_mgr_rwlock);
	assert(gbl_lock_mgr);

	duration<double> time_span;
    /*
	 *single_round(500);
     */
	for (auto x : scale) {
		single_round(x);
	}
}

void single_thread_bench()
{
	insert_locate_bench();
}


int main(int argc, char *argv[])
{
	single_thread_bench();
	return 0;
}


