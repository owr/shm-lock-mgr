#include <pthread.h>
#include <thread>
#include <unistd.h>
#include <sys/wait.h>

#include <iostream>
using namespace std;

#define N_THRS 3
static volatile int glob = 0;

pthread_rwlock_t rwlock;

static void *threadFunc0()
{
	cout << "reader try to get lock" << endl;
	pthread_rwlock_rdlock(&rwlock);
	cout << "reader got lock" << endl;
	sleep(1);
	pthread_rwlock_unlock(&rwlock);
	cout << "reader release lock" << endl;
    return NULL;

}

static void *threadFunc1()
{
	usleep(100000);
	cout << "writer try to get lock" << endl;
	pthread_rwlock_wrlock(&rwlock);
	cout << "writer got lock" << endl;
	pthread_rwlock_unlock(&rwlock);
	cout << "writer release lock" << endl;
    return NULL;

}

void shared_thread()
{
	thread thds[N_THRS];
	thds[0] = thread(threadFunc0);
	thds[1] = thread(threadFunc1);
	thds[2] = thread(threadFunc0);

	thds[0].join();
	thds[1].join();
	thds[2].join();
}

void shared_process()
{
	if (!fork()) {
		cout << "child writer try to get lock" << endl;
		pthread_rwlock_wrlock(&rwlock);
		cout << "child writer got lock" << endl;
		sleep(1);
		pthread_rwlock_unlock(&rwlock);
		cout << "release lock" << endl;
		exit(0);
	} else {
		usleep(100000);
		cout << "writer try to get lock" << endl;
		pthread_rwlock_wrlock(&rwlock);
		cout << "writer got lock" << endl;
		pthread_rwlock_unlock(&rwlock);
		cout << "writer release lock" << endl;
		int status;
		wait(&status);
	}
}


int main(int argc, char *argv[])
{
	//shared_thread();
	shared_process();

	return 0;
}

