#include <boost/thread/shared_mutex.hpp>
#include <thread>
using namespace std;
typedef boost::shared_mutex Mutex;


static volatile int glob = 0;
Mutex mutex;

static void *threadFunc0()
{
	cout << "reader try to get lock" << endl;
	mutex.lock_shared();
	cout << "reader got lock" << endl;
	sleep(1);
	mutex.unlock_shared();
	cout << "reader release lock" << endl;
    return NULL;

}

static void *threadFunc1()
{
	usleep(100000);
	cout << "writer try to get lock" << endl;
	mutex.lock();
	cout << "writer got lock" << endl;
	mutex.unlock();
	cout << "writer release lock" << endl;
    return NULL;

}


#define N_THRS 3

int main(int argc, char *argv[])
{
	thread thds[N_THRS];
	thds[0] = thread(threadFunc0);
	thds[1] = thread(threadFunc1);
	thds[2] = thread(threadFunc0);


	thds[0].join();
	thds[1].join();
	thds[2].join();


	return 0;
}

