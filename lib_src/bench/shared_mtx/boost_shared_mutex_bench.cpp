#include <boost/thread/shared_mutex.hpp>
#include <thread>
using namespace std;
typedef boost::shared_mutex Mutex;


static volatile int glob = 0;
Mutex mutex;

static void *threadFunc(void *arg)
{
    int loops = *((int *) arg);
    int loc, j, s;

    for (j = 0; j < loops; j++) {
        mutex.lock();

        loc = glob;
        loc++;
        glob = loc;

        mutex.unlock();
    }
    return NULL;

}

#define N_THRS 2

int main(int argc, char *argv[])
{
	thread thds[N_THRS];
	int n = 10000000;

	for (int i = 0; i < N_THRS; ++i) {
		thds[i] = thread(threadFunc, &n);
	}

	for (int i = 0; i < N_THRS; ++i)
		thds[i].join();

	cout << glob << endl;

	return 0;
}

