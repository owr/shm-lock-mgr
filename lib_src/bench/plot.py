from astropy.io import ascii
import matplotlib.pyplot as plt
import pylab
import os
import re
import glob
from astropy.io import ascii

#all_res_files = glob.glob("./res_dir/*.csv")
#all_res_files = [
#"./res_dir/res_1000.csv",
#"./res_dir/res_2000.csv",
#"./res_dir/res_3000.csv",
#"./res_dir/res_4000.csv",
#"./res_dir/res_5000.csv"
#]

all_res_files = [
"./res_dir/res_10000.csv",
"./res_dir/res_20000.csv",
"./res_dir/res_40000.csv",
"./res_dir/res_80000.csv",
"./res_dir/res_160000.csv",
"./res_dir/res_320000.csv",
"./res_dir/res_640000.csv",
"./res_dir/res_1280000.csv",
"./res_dir/res_2560000.csv",
"./res_dir/res_5120000.csv",

#"./res_dir/res_1000.csv",
#"./res_dir/res_2000.csv",
#"./res_dir/res_3000.csv",
#"./res_dir/res_4000.csv",
#"./res_dir/res_5000.csv"
]

handles = []

hd, = plt.plot([0], label = 'Lock set size', color = 'w')
handles.append(hd)

for f in all_res_files:
	tbl = ascii.read(f)
	scale = f.split('_')[2].split('.')[0]
	hd1, = plt.plot(tbl['rlock_latency'], label = scale)
	hd2, = plt.plot(tbl['wlock_latency'], label = scale)
	#handles.append(hd1)
	handles.append(hd2)

plt.xlabel('Number of threads')
plt.ylabel('Lock latency (nano secs)')
plt.title('Read/Write lock effeciency under various lock sets')
plt.grid(True)
plt.annotate('Read lock latency', xy=(600, 5.5e8), xytext=(650, 1.0e9),
		arrowprops=dict(facecolor='red', shrink=0.05),)

plt.annotate('Write lock latency', xy=(640, 3.5e8), xytext=(750, 2e8),
		arrowprops=dict(facecolor='blue', shrink=0.05),)


plt.legend(handles = handles, loc = 0)
plt.show()



