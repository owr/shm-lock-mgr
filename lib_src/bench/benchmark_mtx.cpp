#include "../api.h"
//#include "common.h"
#include <string.h>
#include <assert.h>
#include <wait.h>
#include <algorithm>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ratio>
#include <map>
#include <mutex>
#include <thread>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <chrono>
#include <atomic>
//#include <boost/thread/shared_mutex.hpp>

/**
 * For global shm management, the last one who
 * destroy the lock manager should delete the shm
 * segment
 */
struct ShmRWLock {
	pthread_rwlock_t rw_lock;
	std::atomic<int32_t> ref_count;
};

extern struct ShmRWLock *gbl_seg_lck;

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock::time_point Time;
typedef duration<double, nano> Span;
typedef int(*func)(uint32_t);

// nanoseconds, simulate work time, SSD latency 0.1ms = 100us
#define SLEEP_TIME 100'000

// protect cout buffer
std::mutex mtx;
std::mutex _mtx;
//boost::shared_mutex entry_mutex;
//std::shared_mutex sh_mtx; //use boost's or c++14

// bench multiple times, to increase accuracy
//#define BENCH_SET_SIZE 1000
#define BENCH_SET_SIZE 100

enum bench_entity_type {
	INSERT_BENCH = 0,
	SEARCH_BENCH,
	RDLOCK_BENCH,
	WRLOCK_BENCH
};

struct result_t {
	// -1 is single thread
	int thr_nr;
	int scale;
	int type;
	double dura_span;
};

std::vector<int> scale;
// hold initial lock set
std::vector<int> init_rand_locks;
// hold bench lock set
std::vector<int> bench_rand_vec;
// helper structure
std::unordered_set<int> init_rand_set;
// helper structure
std::unordered_set<int> bench_rand_set;
std::vector<result_t> bench_result;

// scale of numer of locks
static void init_scale()
{
	scale.clear();
	// first case is for warming up
	int i = 5;
	for (int j = 0; j < 7; ++j) {
		std::vector<int> v;
		scale.push_back(i);
		i *= 10;
	}
}

// generate random bench set from testbed
static void fetch_rand_vec(vector<int> &v)
{
	v.clear();
	while (v.size() < BENCH_SET_SIZE)
		v.push_back(init_rand_locks[rand() % init_rand_locks.size()]);
}

static void get_rand_vec(vector<int> &v)
{
	v.clear();
	while (v.size() < BENCH_SET_SIZE)
		v.push_back(rand());
}

static void init()
{
	init_scale();
}

static double insert_bench(vector<int> &v)
{
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_insert_page(v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

static double search_bench()
{
	vector<int> &_v = bench_rand_vec;
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_search_page(_v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

uint64_t m = 0;

void acc_m()
{
	++m;
}

static double rlock_bench()
{
	vector<int> &_v = bench_rand_vec;
	Time t1, t2;
	t1 = high_resolution_clock::now();
	int ret;
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_enter_mutex(_v[i]);
		struct timespec req = { .tv_sec = 0,
			.tv_nsec = SLEEP_TIME};
		struct timespec rem;

		mgr_enter_mutex(_v[i]);
		ret = nanosleep(&req, &rem);
		if (ret) {
			perror("nanosleep");
			exit(-1);
		}
		mgr_leave_mutex(_v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

static double wlock_bench()
{

	vector<int> &_v = bench_rand_vec;
	Time t1, t2;
	int ret;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		struct timespec req = { .tv_sec = 0,
			.tv_nsec = SLEEP_TIME};
		struct timespec rem;

		mgr_enter_mutex(_v[i]);
		ret = nanosleep(&req, &rem);
		if (ret) {
			perror("nanosleep");
			exit(-1);
		}
		if (i == 2)
			acc_m();
		mgr_leave_mutex(_v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

// single thread
void bench_entity_single(int _scale, int _bench_type)
{
	bench_result.clear();
	Time t1, t2;
	assert(bench_rand_set.size() == BENCH_SET_SIZE);

	vector<int> v;
	copy(begin(bench_rand_set), end(bench_rand_set), back_inserter(v));

	string _bench_name;
	double _duration;
	switch (_bench_type) {
	case INSERT_BENCH:
		_duration = insert_bench(v);
		_bench_name = "insert";
		break;
	case SEARCH_BENCH:
		_duration = search_bench();
		_bench_name = "search";
		break;
	case RDLOCK_BENCH:
		_duration = rlock_bench();
		_bench_name = "rlock";
		break;
	case WRLOCK_BENCH:
		_duration = wlock_bench();
		_bench_name = "wlock";
		break;
	default:
		break;
	}

	mtx.lock();
	if (_scale > 5) {
		printf("%s- %d-bench_tp-%d-: %lf nanoseconds\n", _bench_name.c_str(), _scale,
			   _bench_type, _duration/BENCH_SET_SIZE);
	}
	fflush(stdout);
	mtx.unlock();
}

void bench_entity_multi(int _scale, int _bench_type)
{
	lock_mgr_init();
	bench_result.clear();
	assert(bench_rand_set.size() == BENCH_SET_SIZE);
	vector<int> v;
	copy(begin(bench_rand_set), end(bench_rand_set), back_inserter(v));

	string _bench_name;
	double _duration = 0;

	switch (_bench_type) {
	case INSERT_BENCH:
		_duration = insert_bench(v);
		_bench_name = "insert";
		break;
	case SEARCH_BENCH:
		_duration = search_bench();
		_bench_name = "search";
		break;
	case RDLOCK_BENCH:
		_duration = rlock_bench();
		_bench_name = "rlock";
		break;
	case WRLOCK_BENCH:
		_duration = wlock_bench();
		_bench_name = "wlock";
		break;
	default:
		break;
	}

	mtx.lock();
	if (_scale > 5) {
		printf("%s- %d-bench_tp-%d-: %lf nanoseconds\n", _bench_name.c_str(), _scale,
			   _bench_type, _duration/BENCH_SET_SIZE);
	}
	fflush(stdout);
	mtx.unlock();
}

static void single_round_prepare(int _scale)
{
	srand(113);
	init_rand_set = unordered_set<int>();
	init_rand_locks.clear();

	// generate testbed sets, generate # _scale locks into lock set init_rand_nums
	while (init_rand_set.size() < _scale) {
		int rd = rand() % INT_MAX;
		init_rand_set.insert(rd);
	}

	// generate bench sets, randomly fetch BENCH_SET_SIZE locks from the testbed set
	// to bench the performance of the lock manager
	while (bench_rand_set.size() < BENCH_SET_SIZE) {
		int rd = rand() % INT_MAX;
		if (init_rand_set.find(rd) == init_rand_set.end()) {
			bench_rand_set.insert(rd);
		}
	}

	copy(begin(init_rand_set), end(init_rand_set), back_inserter(init_rand_locks));

	// init the lock test bed
	for (int i = 0; i < _scale; ++i) {
		mgr_insert_page(init_rand_locks[i]);
	}
}


void single_thread_bench()
{
	init();
	for (auto x : scale) {
		single_round_prepare(x);
		fetch_rand_vec(bench_rand_vec);
		bench_entity_single(x, 0);
		bench_entity_single(x, 1);
		bench_entity_single(x, 2);
		bench_entity_single(x, 3);
		//lock_mgr_destroy();
	}

}

static void multi_thread_iter(int th_nr, int scale, int _bench_type)
{
	lock_mgr_init();
	bench_rand_vec.clear();
	vector<thread> threads(th_nr);
	single_round_prepare(scale);
	// same random bench vec for the single round
	fetch_rand_vec(bench_rand_vec);
	//get_rand_vec(bench_rand_vec);

	for (int i = 0; i < th_nr; ++i) {
		threads[i] = thread(bench_entity_multi, scale, _bench_type);
	}

	for (int i = 0; i < th_nr; ++i) {
		threads[i].join();
	}
	// cout << m << endl;
	//dump_mrg();
}

static void multi_thread_iter_mixed(int th_nr, int scale)
{
	lock_mgr_init();
	bench_rand_vec.clear();
	vector<thread> threads(th_nr);
	single_round_prepare(scale);
	// same random bench vec for the single round
	fetch_rand_vec(bench_rand_vec);
	//get_rand_vec(bench_rand_vec);


	for (int i = 0; i < th_nr; ++i) {
		if (i % 2)
			// writer
			threads[i] = thread(bench_entity_multi, scale, 2);
		else
			// reader
			threads[i] = thread(bench_entity_multi, scale, 3);
	}

	for (int i = 0; i < th_nr; ++i) {
		threads[i].join();
	}
}

int _i = 0;
static void lock_test_entity()
{
	lock_mgr_init();
	int ret;
	pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
	for (int j = 0; j < 100; ++j) {
		struct timespec req = { .tv_sec = 0,
			.tv_nsec = SLEEP_TIME};
		struct timespec rem;
		_i = _i + 3;

		ret = nanosleep(&req, &rem);
		if (ret) {
			perror("nanosleep");
			exit(-1);
		}
	}
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
}

// smoke test
static void mgr_lock_test(int th_nr)
{
	vector<thread> threads(th_nr);
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < th_nr; ++i) {
		threads[i] = thread(lock_test_entity);
	}

	for (int i = 0; i < th_nr; ++i) {
		threads[i].join();
	}
	t2 = high_resolution_clock::now();
	printf("%f\n", duration_cast<Span>(t2 - t1).count());
	printf("%d\n", _i);
}

int main(int argc, char *argv[])
{
	std::cout.sync_with_stdio(false);
	std::cin.sync_with_stdio(false);
	lock_mgr_destroy();
	lock_mgr_init();

	if (argc < 3) {
		perror("wrong paramater!");
		printf("1-thnr, 2-scale, 3-bench_type");
		exit(-1);
	}
	// number of threads
	int thnr = atoi(argv[1]);
	// numer of locks
	int scale = atoi(argv[2]);
	//
	int bench_type = atoi(argv[3]);
	//mgr_lock_test(thnr);
	//multi_thread_iter(thnr, scale, bench_type);
    /*
	 *multi_thread_iter_mixed(thnr, scale);
     */
	single_thread_bench();
	return 0;
}


