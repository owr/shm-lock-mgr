#!/usr/bin/python
import re
import time
import fileinput

fs_types = {
        "default" : 1,
        "lzo" : 1,
        "zlib" : 1,
        "lz4" : 1,
        "lz4hc" : 1,
        }

#def proc_fs_block():

fs_flag = ""
rw = ""
job_nr = 0
dio = 0
bs = 0
cpu_utl = 0
dstat_res = {}
dstat_file = "./dstat_ssd"

def _load_dstat():
    for l in fileinput.input(dstat_file):
        _l = l.strip().split('|')
        _l = map(lambda s: s.strip(), _l)
        if _l[0] == "time":
            continue
        _t = "2015-" + _l[0]
        # in time array 
        st = time.strptime(_t, "%Y-%d-%m %H:%M:%S")
        # in seconds
        ts = int(time.mktime(st))
        dstat_res[ts] = _l[1].split()

def cal_res():
    for line in fileinput.input():
        _line = line.strip().split()
        if len(_line) == 3:
            if _line[2] in fs_types.keys():
                fs_flag = _line[2]
            else:
                print "error!!!"
                break

        if _line[0] == "fio":
            rw = _line[2].split('=')[1]
            job_nr = _line[3].split('=')[1]
            dio = _line[4].split('=')[1]
            bs = _line[7].split('=')[1].strip('K')
            #print fs_flag, rw, job_nr, dio, bs
        if _line[0] == "rrrr:":
            t_line = line.strip().split(': ')
            _time = t_line[4]
            # in time array 
            st_arr = time.strptime(_time, "%a %b %d %H:%M:%S %Y")
            # in seconds
            ts = int(time.mktime(st_arr))
            idl_cpu = 0
            # skip warm up time
            ts += 4
            for i in range(5):
                _r = dstat_res[ts + i]
                #if int(_r[1]) >= 90:
                    #print _r, fs_flag, dio, rw, job_nr, bs, avg_thr
                idl_cpu = idl_cpu + int(_r[2])
            cpu_utl = 100 - idl_cpu/5.0

        if _line[0] == "READ:":
            avg_thr = line.strip().split(',')[1].split('=')[1].strip("KB/s")
            # KB/s
            if avg_thr == avg_thr.strip('M'): 
                avg_thr = float(avg_thr) / 1024.0
            else:
            # MB/s
                avg_thr = float(avg_thr.strip('M')) 
            print fs_flag, dio, rw, job_nr, bs, avg_thr, cpu_utl

_load_dstat()
cal_res()





