#include "api.h"
#include "common.h"
#include <string.h>
#include <assert.h>
#include <wait.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ratio>
#include <map>
#include <mutex>
#include <thread>
#include <vector>
#include <iostream>
#include <chrono>
#include <boost/thread/shared_mutex.hpp>


using namespace std;
using namespace std::chrono;

typedef high_resolution_clock::time_point Time;
typedef duration<double, nano> Span;
typedef int(*func)(uint32_t);

// protect cout buffer
std::mutex mtx;
std::mutex _mtx;
boost::shared_mutex entry_mutex;
//std::shared_mutex sh_mtx; //use boost's or c++14

//#define BENCH_SET_SIZE 1000
#define BENCH_SET_SIZE 100

enum bench_entity_type {
	INSERT_BENCH = 0,
	SEARCH_BENCH,
	RDLOCK_BENCH,
	WRLOCK_BENCH
};

struct result_t {
	// -1 is single thread
	int thr_nr;
	int scale;
	int type;
	double dura_span;
};

vector<int> scale;
vector<int> init_rand_nums_v;
vector<int> bench_rand_vec;
map<int, bool> init_rand_nums;
map<int, bool> bench_rand_nums;
vector<result_t> bench_result;

static void init_scale()
{
	scale.clear();
	// first case is for warming up
	int i = 5;
	for (int j = 0; j < 7; ++j) {
		vector<int> v;
		scale.push_back(i);
		i *= 10;
	}
}

static void fetch_rand_vec(vector<int> &v)
{
	while (v.size() < BENCH_SET_SIZE)
		v.push_back(init_rand_nums_v[rand() % init_rand_nums_v.size()]);
}

static void get_rand_vec(vector<int> &v)
{
	while (v.size() < BENCH_SET_SIZE)
		v.push_back(rand());
}

static void init()
{
	init_scale();
}

static double insert_bench(vector<int> &v)
{
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_insert_page(v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

static double search_bench()
{
	vector<int> &_v = bench_rand_vec;
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_search_page(_v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

uint64_t m = 0;

void acc_m()
{
	++m;
    /*
	 *for (int i =0; i < 100000; ++i)
	 *    ++m;
     */
}

static double rlock_bench()
{
	vector<int> &_v = bench_rand_vec;
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_rpage_lock(_v[i]);
		usleep(1000);
		mgr_rpage_unlock(_v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

static double wlock_bench()
{
	vector<int> &_v = bench_rand_vec;
	Time t1, t2;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < BENCH_SET_SIZE; ++i) {
		mgr_wpage_lock(_v[i]);
		usleep(1000);
		if (i == 2)
			acc_m();
		mgr_wpage_unlock(_v[i]);
	}
	t2 = high_resolution_clock::now();
	return duration_cast<Span>(t2 - t1).count();
}

void bench_entity(int _scale, int _bench_type)
{
	bench_result.clear();
	Time t1, t2;
	vector<int> v(BENCH_SET_SIZE, 0);
	int i = 0;
	assert(bench_rand_nums.size() == BENCH_SET_SIZE);

	for (auto &x : bench_rand_nums) {
		v[i++] = x.first;
	}

	string _bench_name;
	double _duration;
	switch (_bench_type) {
	case INSERT_BENCH:
		_duration = insert_bench(v);
		_bench_name = "insert";
		break;
	case SEARCH_BENCH:
		_duration = search_bench();
		_bench_name = "search";
		break;
	case RDLOCK_BENCH:
		_duration = rlock_bench();
		_bench_name = "rlock";
		break;
	case WRLOCK_BENCH:
		_duration = wlock_bench();
		_bench_name = "wlock";
		break;
	default:
		break;
	}

	mtx.lock();
	if (_scale > 5) {
		printf("%s- %d-bench_tp-%d-: %lf nanoseconds\n", _bench_name.c_str(), _scale,
			   _bench_type, _duration/BENCH_SET_SIZE);
	}
	fflush(stdout);
	mtx.unlock();
}

void bench_entity_multi(int _scale, int _bench_type)
{
	lock_mgr_init();
	bench_result.clear();
	vector<int> v(BENCH_SET_SIZE, 0);
	int i = 0;
	assert(bench_rand_nums.size() == BENCH_SET_SIZE);

	for (auto &x : bench_rand_nums) {
		v[i++] = x.first;
	}

	string _bench_name;
	double _duration = 0;

	switch (_bench_type) {
	case INSERT_BENCH:
		_duration = insert_bench(v);
		_bench_name = "insert";
		break;
	case SEARCH_BENCH:
		_duration = search_bench();
		_bench_name = "search";
		break;
	case RDLOCK_BENCH:
		_duration = rlock_bench();
		_bench_name = "rlock";
		break;
	case WRLOCK_BENCH:
		_duration = wlock_bench();
		_bench_name = "wlock";
		break;
	default:
		break;
	}

	mtx.lock();
	if (_scale > 5) {
		printf("%s- %d-bench_tp-%d-: %lf nanoseconds\n", _bench_name.c_str(), _scale,
			   _bench_type, _duration/BENCH_SET_SIZE);
	}
	fflush(stdout);
	mtx.unlock();
}

static void single_round_prepare(int _scale)
{
	srand(113);
	init_rand_nums.clear();
	init_rand_nums_v.clear();

	while (init_rand_nums.size() < _scale) {
		int rd = rand() % INT_MAX;
		init_rand_nums[rd] = true;
	}

	while (bench_rand_nums.size() < BENCH_SET_SIZE) {
		int rd = rand() % INT_MAX;
		if (init_rand_nums.find(rd) == init_rand_nums.end()) {
			bench_rand_nums[rd] = true;
		}
	}

	for (auto &x : init_rand_nums) {
		init_rand_nums_v.push_back(x.first);
	}

	for (int i = 0; i < _scale; ++i) {
		mgr_insert_page(init_rand_nums_v[i]);
	}
}


void single_thread_bench()
{
	init();
	for (auto x : scale) {
		single_round_prepare(x);
		bench_entity(x, 0);
		bench_entity(x, 1);
		bench_entity(x, 2);
		bench_entity(x, 3);
		//lock_mgr_destroy();
	}

}

static void multi_thread_iter(int th_nr, int scale, int _bench_type)
{
	lock_mgr_init();
	bench_rand_vec.clear();
	vector<thread> threads(th_nr);
	single_round_prepare(scale);
	// same random bench vec for the single round
	fetch_rand_vec(bench_rand_vec);
	//get_rand_vec(bench_rand_vec);

	for (int i = 0; i < th_nr; ++i) {
		threads[i] = thread(bench_entity_multi, scale, _bench_type);
	}

	for (int i = 0; i < th_nr; ++i) {
		threads[i].join();
	}
	// cout << m << endl;
	//dump_mrg();
}

static void multi_thread_iter_mixed(int th_nr, int scale)
{
	lock_mgr_init();
	bench_rand_vec.clear();
	vector<thread> threads(th_nr);
	single_round_prepare(scale);
	// same random bench vec for the single round
	fetch_rand_vec(bench_rand_vec);
	//get_rand_vec(bench_rand_vec);


	for (int i = 0; i < th_nr; ++i) {
		if (i % 2)
			// writer
			threads[i] = thread(bench_entity_multi, scale, 2);
		else
			// reader
			threads[i] = thread(bench_entity_multi, scale, 3);
	}

	for (int i = 0; i < th_nr; ++i) {
		threads[i].join();
	}
}

int _i = 0;
static void lock_test_entity()
{
	lock_mgr_init();
	pthread_rwlock_wrlock(&gbl_seg_lck->seg_lock);
	for (int j = 0; j < 100; ++j) {
		_i = _i + 3;
		usleep(100);
	}
	pthread_rwlock_unlock(&gbl_seg_lck->seg_lock);
}

static void mgr_lock_test(int th_nr)
{
	vector<thread> threads(th_nr);
	for (int i = 0; i < th_nr; ++i) {
		threads[i] = thread(lock_test_entity);
	}

	for (int i = 0; i < th_nr; ++i) {
		threads[i].join();
	}
	cout << _i << endl;
}

int main(int argc, char *argv[])
{
    /*
	 *lock_mgr_destroy();
     */
	lock_mgr_init();

	if (argc < 3) {
		perror("wrong paramater!");
		printf("1-thnr, 2-scale, 3-bench_type");
		exit(-1);
	}
	int thnr = atoi(argv[1]);
	int scale = atoi(argv[2]);
	int bench_type = atoi(argv[3]);
    /*
	 *mgr_lock_test(thnr);
     */
	//multi_thread_iter(thnr, scale, bench_type);
	multi_thread_iter_mixed(thnr, scale);
	//single_thread_bench();
	return 0;
}


