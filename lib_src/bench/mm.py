from astropy.io import ascii
import matplotlib.pyplot as plt
import pylab
import os
import re
import glob
from astropy.io import ascii


line_up, = plt.plot([1,2,3], label='Line 2')
line_down, = plt.plot([3,2,1], label='Line 1')
plt.legend(handles=[line_up, line_down])
plt.show()
