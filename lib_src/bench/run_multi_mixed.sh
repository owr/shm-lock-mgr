scales="
1000
2000
3000
4000
5000
10000
20000
40000
80000
160000
320000
640000
1280000
2560000
5120000"

types="
1
2
3
0"

for s in `seq 50`
do
	s=$((s*100))
	echo > ./res_dir/res_${s}
	for i in `seq 1000`
	do
		echo TIME`date +%F_%T` >> ./res_dir/res_${s} 
		./benchmark $i $s $t | awk -v i="$i" -F'[: ]' '{print i, $2, $3, $4 }' >> ./res_dir/res_${s} 
	done
done

