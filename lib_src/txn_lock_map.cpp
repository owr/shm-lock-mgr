#include "shm-lockmap.hpp"
#include "deadlock_graph.hpp"
#include <glog/logging.h>


#ifdef __cplusplus
extern "C" {
#endif

TXNLockMap *gbl_txn_lock_map = nullptr;

static PageVec *_add_txn_entity(TXNId txn_id)
{
	PageVec *_page_lock_vec;
	std::string obj_name = std::to_string(txn_id) + "_txn";

	_page_lock_vec = gbl_mem_seg->template construct<PageVec>
		(obj_name.c_str())(gbl_mem_seg->get_segment_manager());
	return _page_lock_vec;
}

/**
 * We can call mgr_enter_mutex before this
 * function, and call this function in our
 * program
 */
int add_lock_to_txn(TXNId txn_id, Pgno pgno)
{
	PageVec *_page_lock_vec = nullptr;

	auto &obj = *gbl_txn_lock_map;
	// to be optimized, with another rd lock, seperate from gbl_seg_lck
	pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	auto exists = obj.count(txn_id);
	if (exists) {
		auto iter = obj.find(txn_id);
		_page_lock_vec = &iter->second;
	}
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	// else we need to add txn_id into map, need write lock
	if (!exists) {
		pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
		_page_lock_vec = _add_txn_entity(txn_id);
		obj.insert(std::move(TXNLockMapValueType(txn_id,
				   std::move(*_page_lock_vec))));
		pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
	}

	// retract the object again to insert, since
	// the about move semantics are not supported
	pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	auto iter = obj.find(txn_id);
	_page_lock_vec = &iter->second;
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	assert(_page_lock_vec != nullptr);
#ifdef PGLK_DEBUG
	LOG(INFO) << "pgno: " << pgno;
#endif
	if (std::find(_page_lock_vec->begin(), _page_lock_vec->end(),
			 pgno) == _page_lock_vec->end())
		_page_lock_vec->push_back(pgno);
#ifdef PGLK_DEBUG
	//LOG(INFO) << __func__ << ", size: "<< _page_lock_vec->size();
	dump_locks_on_txn(txn_id);
#endif
	return 0;
}

/**
 * A wrapper function for lock entering and recording
 */
int txn_lock_acquire(TXNId txn_id, Pgno pgno)
{
	int ret = 0;

	pthread_rwlock_rdlock(&gbl_dgraph_lck->rw_lock);
	// to see if txn_id already holding lock pgno
#ifdef DEADLOCK_DEBUG
	dump_dgraph();
#endif
	if (has_edge(-txn_id, pgno)) {
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
		return 0;
	}
	pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);

	// lead_to_circle and dgraph_add_adge should be atomic
	pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
	bool circle = lead_to_circle(-txn_id, pgno);
#ifdef DEADLOCK_DEBUG
	dump_dgraph();
#endif
	LOG(INFO) << "lead_circle: " << circle;
	if (!circle) {
		// txn txn_id is going to acquire lock pgno
		//pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
		dgraph_add_adge(pgno, -txn_id);
#ifdef DEADLOCK_DEBUG
		dump_dgraph();
#endif
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);

		ret = mgr_enter_mutex(pgno);

		// got lock pgno, flip the edge to indicate relationship
		pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
		flip_edge(pgno, -txn_id);
#ifdef DEADLOCK_DEBUG
		dump_dgraph();
#endif
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
		assert(ret == 0);
		ret = add_lock_to_txn(txn_id, pgno);
	} else {
		// release atomic lock
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
		ret = DDLOCK;
	}
	return ret;
}

/**
 * A wrapper function for lock entering and recording
 */
int txn_lock_try_acquire(TXNId txn_id, Pgno pgno)
{
#if 1
	int ret = 0;
	pthread_rwlock_rdlock(&gbl_dgraph_lck->rw_lock);
	// to see if txn_id already holding lock pgno
#ifdef DEADLOCK_DEBUG
	dump_dgraph();
#endif
	if (has_edge(-txn_id, pgno)) {
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
		return 0;
	}
	pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);

	// lead_to_circle and dgraph_add_adge should be atomic
	pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
	bool circle = lead_to_circle(-txn_id, pgno);
#ifdef DEADLOCK_DEBUG
	dump_dgraph();
#endif
	LOG(INFO) << "lead_circle: " << circle;
	if (!circle) {
		// txn txn_id is going to acquire lock pgno
		//pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
		dgraph_add_adge(pgno, -txn_id);
#ifdef DEADLOCK_DEBUG
		dump_dgraph();
#endif
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);

		//ret = mgr_enter_mutex(pgno);
		auto ret = mgr_mutex_keep_trying(pgno, 5'000);

		// got lock pgno, flip the edge to indicate relationship
		pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
		flip_edge(pgno, -txn_id);
#ifdef DEADLOCK_DEBUG
		dump_dgraph();
#endif
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
		if (ret == 0)
			ret = add_lock_to_txn(txn_id, pgno);
		else
			remove_edge(-txn_id, pgno);
	} else {
		// release atomic lock
		pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
		ret = DDLOCK;
	}
	return ret;
#endif
}

/**
 * release a specific lock on txn
 */
int txn_lock_release(TXNId txn_id, Pgno pgno)
{
#ifdef PGLK_DEBUG
	LOG(INFO) << __func__ << ", pgno: " << txn_id << ", " << pgno;
#endif
	auto &obj = *gbl_txn_lock_map;

	pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	auto pr = obj.find(txn_id);
	if (pr == obj.end()) {
		pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
		return 0;
	}

	assert(pr != obj.end());
	assert(pr->first != 0);
	auto &page_vec = pr->second;
	auto iter = std::find(page_vec.begin(), page_vec.end(), pgno);
	if (iter == page_vec.end()) {
		LOG(INFO) << "lock: " << pgno <<
			" does not exists in txn: " << txn_id;
		pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
		return -1;
	}
#ifdef PGLK_DEBUG
	LOG(INFO) << "*iter: " << *iter;
#endif
	auto ret = mgr_leave_mutex(*iter);
	assert(ret == 0);
	assert(page_vec.size() != 0);
	page_vec.erase(iter);
	if (page_vec.size() == 0) {
#ifdef PGLK_DEBUG
		LOG(INFO) << "no lock on page: " << pgno << " erase it";
#endif

#ifdef PGLK_DEBUG
		// could save for reuse, can have performance improvement,
		// so only enable it under debug env
        /*
		 *obj.erase(pr);
		 *destroy_shm_object<PageVec>(gbl_mem_seg, std::to_string(txn_id).c_str());
         */
#endif
	}
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);
#ifdef DEADLOCK_DEBUG
	LOG(INFO) << "before edge remove";
	dump_dgraph();
#endif
	remove_edge(-txn_id, pgno);
#ifdef DEADLOCK_DEBUG
	LOG(INFO) << "after edge remove";
	dump_dgraph();
#endif

	pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);

	return ret;
}


/**
 * release all locks held on txn_id
 */
int txn_lock_release_all(TXNId txn_id)
{
    /*
	 *pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	 *dump_dgraph();
	 *pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
     */

	auto &obj = *gbl_txn_lock_map;

#ifdef PGLK_DEBUG
	LOG(INFO) << " try to get seg_lock";
#endif
	pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
#ifdef PGLK_DEBUG1
	LOG(INFO) << " got seg_lock";
	LOG(INFO) << __func__ << " txn_id: " << txn_id;
	dump_locks_on_txn(txn_id);
#endif

	auto pr = obj.find(txn_id);

	if (pr == obj.end()) {
#ifdef PGLK_DEBUG
		LOG(INFO) << "no lock held on txn: " << txn_id;
#endif
		pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
		return 0;
	}
	//assert(pr != obj.end());
	assert(pr->first != 0);

#ifdef PGLK_DEBUG1
	LOG(INFO) << __func__ << " pr->first: " << pr->first;
#endif

	auto &page_vec = pr->second;
	int ret;
	for (auto &pgno : page_vec) {
#ifdef PGLK_DEBUG1
	LOG(INFO) << __func__ << " pgno: " << pgno;
#endif
		ret = mgr_leave_mutex_all(pgno);
#ifdef PGLK_DEBUG
		//LOG(INFO) << "ret: " << ret;
#endif
		assert(ret == 0);
	}
	page_vec.clear();
#ifdef PGLK_DEBUG
    /*
	 *obj.erase(pr);
	 *destroy_shm_object<PageVec>(gbl_mem_seg, std::to_string(txn_id).c_str());
     */
#endif
release:
#ifdef PGLK_DEBUG
	LOG(INFO) << __func__ << " try to release seg_lock";
	dump_locks_on_txn(txn_id);
#endif
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
#ifdef PGLK_DEBUG
	LOG(INFO) << " released seg_lock";
#endif
	// release locks
	pthread_rwlock_wrlock(&gbl_dgraph_lck->rw_lock);

#ifdef DEADLOCK_DEBUG
	LOG(INFO) << "before remove edges";
	dump_dgraph();
#endif
	remove_edges(-txn_id);
#ifdef DEADLOCK_DEBUG
	LOG(INFO) << "after remove edges";
	dump_dgraph();
#endif
	pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
	return 0;
}

bool is_lock_held(TXNId txn_id, Pgno pgno)
{
	bool is_held = false;
	auto &obj = *gbl_txn_lock_map;

	pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	auto exists = obj.count(txn_id);
	if (exists) {
		auto &page_vec = obj.find(txn_id)->second;
		auto it = std::find(page_vec.begin(), page_vec.end(), pgno);
		if (it != page_vec.end())
			is_held = true;
	}
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	return is_held;
}

void dump_locks_on_txn(TXNId txn_id)
{
	std::string s;
#ifdef PGLK_DEBUG1
	//LOG(INFO) << __func__;
#endif
	auto &obj = *gbl_txn_lock_map;
	auto exists = obj.count(txn_id);
	if (exists) {
		auto iter = obj.find(txn_id);
#ifdef PGLK_DEBUG1
		s += "txn_id, " + std::to_string(txn_id)
			+ ", size, " + std::to_string(iter->second.size())
			+ ", locks: {";
#endif
		for (auto &x: iter->second)
			s += std::to_string(x) + ", ";
		s += "}";
	} else {
#ifdef PGLK_DEBUG1
		s += "no lock held on txn: " + std::to_string(txn_id);
#endif
	}
#ifdef PGLK_DEBUG1
	LOG(INFO) << s;
#endif
}



#ifdef __cplusplus
}
#endif
