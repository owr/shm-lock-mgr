#ifndef _HASH_TBL_H_
#define _HASH_TBL_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * apis about hashtable handling
 */
/**
 * create a hash table, and return a handle
 */
void* hashtbl_create();

/**
 * add a new element into the hashtable
 */
void hashtbl_add(void *hash_tbl, uint32_t key, void *val);

/**
 * to see if a key exists in the hashtable
 */
void *hashtbl_find(void *hash_tbl, uint32_t key);

/**
 * delete one element from hashtable
 */
void hashtbl_del(void *hash_tbl, uint32_t key);

/**
 * free the hashtable
 */
void hashtbl_free(void *hash_tbl);

/**
 * dump keys in the hashtable, for debug
 */
void hashtbl_dump(void *hash_tbl);

/**
 * test api
 */
void hash_test();

#ifdef __cplusplus
}
#endif


#endif
