#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <wait.h>
#include "../page_lock.h"


void multi_process_test()
{
	pid_t pid;

	if ((pid = fork()) == 0) {
		// child
		lock_mgr_init();
		// OK
		usleep(1000);
		mgr_enter_mutex(43);
		mgr_leave_mutex(43);

		mgr_enter_mutex(43);
		usleep(1000);
        /*
		 *mgr_leave_mutex(43);
         */
		exit(0);
	} else {
		// parent
		lock_mgr_init();

		mgr_enter_mutex(43);
		usleep(10000);
		mgr_enter_mutex(43);
		usleep(10000);
		mgr_leave_mutex(43);
		mgr_leave_mutex(43);
		usleep(1000);

		mgr_enter_mutex(43);
		usleep(1000);
		mgr_leave_mutex(43);
	}

	int status;
	wait(&status);

}

int i = 0;
void *func(void *ptr)
{
	lock_mgr_init();
	int ret = 0;
	ret = mgr_enter_mutex(323);
	printf("ret_mtx: %d\n", ret);
	assert(ret == 0);

	ret = mgr_enter_mutex(323);
	printf("ret_mtx: %d\n", ret);
	assert(ret == 0);

	ret = mgr_leave_mutex(323);
	printf("ret_mtx: %d\n", ret);
	assert(ret == 0);

	ret = mgr_leave_mutex(323);
	printf("ret_mtx: %d\n", ret);
	assert(ret == 0);
}

#define NR_THREADS 10
void multi_thread_test()
{
#if 0
	pthread_t tids[NR_THREADS];
	int s;

	for (int i = 0; i < NR_THREADS; ++i) {
		s = pthread_create(&tids[i], NULL, func, NULL);
		if (s != 0) {
			perror("thread create error");
			exit(-1);
		}
	}

	for (int i = 0; i < NR_THREADS; ++i) {
		s = pthread_join(tids[i], NULL);
		if(s)
			perror("pthread_join");
	}
#endif
}


int main(int argc, char *argv[])
{
	lock_mgr_destroy();

	lock_mgr_init();
#if 1
	printf("ret: %d\n", mgr_enter_mutex(323));
	printf("ret: %d\n", mgr_enter_mutex(323));
	printf("tid: %ld, ================\n\n", gettid());
	mgr_leave_mutex(323);
	mgr_leave_mutex(323);
#endif

	//multi_thread_test();
	//multi_process_test();

#if 0
    /*
	 *mgr_rpage_lock(3);
	 *mgr_rpage_unlock(3);
     */

    /*
	 *mgr_insert_page(3280);
     */
	mgr_rpage_lock(3280);
	mgr_rpage_lock(3280);
	mgr_rpage_unlock(3280);
	mgr_rpage_unlock(3280);

	mgr_enter_mutex(3280);
	mgr_enter_mutex(3280);

	mgr_rpage_lock(3280);

    /*
	 *mgr_enter_mutex(3280);
     */

    /*
	 *mgr_leave_mutex(3280);
     */
#endif
	lock_mgr_destroy();

	return 0;
}
