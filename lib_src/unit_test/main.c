#include "api.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

struct mystruct {
	int key;
	char attr[10];
};


#if 1
void hash_test1()
{
	struct mystruct mm[3];
	mm[0].key = 0;
	strcpy(mm[0].attr, "zero");
	mm[1].key = 1;
	strcpy(mm[1].attr, "first");
	mm[2].key = 2;
	strcpy(mm[2].attr, "second");

	void *hash_tbl = hashtbl_create();
	hashtbl_add(hash_tbl, 0, &mm[0]);
	hashtbl_add(hash_tbl, 1, &mm[1]);
	hashtbl_add(hash_tbl, 2, &mm[2]);

	struct mystruct *_f = (struct mystruct*)hashtbl_find(hash_tbl, 1);
	printf("%s\n", _f->attr);
	_f = (struct mystruct*)hashtbl_find(hash_tbl, 10);
	printf("%p\n", _f);

}
#endif

void page_lock_test()
{
	lock_mgr_destroy();
	lock_mgr_init();
	printf("%p\n", gbl_lock_mgr);
	//lock_mgr_destroy();
}

void page_rdlock_test()
{

	lock_mgr_destroy();
	lock_mgr_init();
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
    /*
	 *dump_mrg();
     */

	if (!fork()) {
		lock_mgr_init();
		do_read_page(1, 1, 0);
		lock_mgr_destroy_segvar();
		exit(0);
	} else {
		do_read_page(1, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();

}

void page_wrlock_test()
{

	lock_mgr_destroy();

	lock_mgr_init();
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	if (!fork()) {
		lock_mgr_init();
		do_write_page(1, 1, 0);
		//lock_mgr_destroy_segvar();
		exit(0);
	} else {
		do_write_page(1, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}

void page_rwlock_test()
{

	lock_mgr_destroy();
	lock_mgr_init();
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	if (!fork()) {
		lock_mgr_init();
		do_read_page(1, 1, 0);
		lock_mgr_destroy_segvar();
		exit(0);
	} else {
		do_write_page(1, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}



int main(int argc, char *argv[])
{
	//hash_test1();
	lock_mgr_destroy();
	op();
    /*
	 *page_lock_test();
     */
    /*
	 *page_rdlock_test();
     */
	page_rwlock_test();
    /*
	 *page_wrlock_test();
     */
	return 0;
}


