#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

#define MUTEX "/mutex_lock"
#define MESSAGE "/msg"
#define OKTOWRITE "/condwrite"

void test()
{
	pthread_cond_t *cond;
	pthread_rwlock_t *mtx;
	char *msg;
	int des_cond, des_msg, des_mtx;
	int mode = S_IRWXU | S_IRWXG;

	des_mtx = shm_open(MUTEX, O_CREAT | O_RDWR | O_TRUNC, mode);
	if (des_mtx < 0) {
		perror("shm_open failed");
		exit(-1);
	}

	if (ftruncate(des_mtx, sizeof(pthread_rwlock_t)) == -1) {
		perror("ftruncate failed");
		exit(-1);
	}

	mtx = (pthread_rwlock_t*) mmap(NULL, sizeof(pthread_rwlock_t),
								  PROT_READ | PROT_WRITE, MAP_SHARED,
								  des_mtx, 0);
	if (mtx == MAP_FAILED) {
		perror("map failed");
		exit(-1);
	}

	des_cond = shm_open(OKTOWRITE, O_CREAT | O_RDWR | O_TRUNC, mode);
	if (des_cond < 0) {
		perror("shm_open failed on des_cond");
		exit(-1);
	}

	if (ftruncate(des_cond, sizeof(pthread_cond_t)) == -1) {
		perror("ftruncate failed on des_cond");
		exit(-1);
	}

	cond = (pthread_cond_t*) mmap(NULL, sizeof(pthread_cond_t),
								  PROT_READ | PROT_WRITE, MAP_SHARED,
								  des_cond, 0);
	if (cond == MAP_FAILED) {
		perror("map failed on cond");
		exit(-1);
	}

	/***mutex***/
	pthread_rwlockattr_t mtxattr;
	//pthread_mutexattr_settype(&mtxattr, PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_rwlockattr_setpshared(&mtxattr, PTHREAD_PROCESS_SHARED);
	pthread_rwlock_init(mtx, &mtxattr);

	/***cond***/
    /*
	 *pthread_condattr_t condattr;
	 *pthread_condattr_setpshared(&condattr, PTHREAD_PROCESS_SHARED);
	 *pthread_cond_init(cond, &condattr);
     */

	/** test **/
	// child
	if (!fork()) {
		pthread_rwlock_wrlock(mtx);

		sleep(2);
        /*
		 *pthread_cond_signal(cond);
		 *printf("child send signal\n");
         */

		pthread_rwlock_unlock(mtx);
		exit(0);
		// parent
	} else {
		printf("parent waiting on cond\n");
		sleep(1);
		pthread_rwlock_wrlock(mtx);

        /*
		 *pthread_cond_wait(cond, mtx);
         */

		pthread_rwlock_unlock(mtx);

		printf("signaled by child, wakeup!\n");
		int status;
		wait(&status);

/*
 *        pthread_condattr_destroy(&condattr);
 *        pthread_rwlockattr_destroy(&mtxattr);
 *        pthread_rwlock_destroy(mtx);
 *        pthread_cond_destroy(cond);
 *
 */
		shm_unlink(OKTOWRITE);
		shm_unlink(MESSAGE);
		shm_unlink(MUTEX);
	}
}

int main(int argc, char *argv[])
{
	shm_unlink(OKTOWRITE);
	shm_unlink(MESSAGE);
	shm_unlink(MUTEX);

	test();
	return 0;
}

