#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <sys/wait.h>
#include <vector>
#include <typeinfo>
//using namespace std;

int main()
{
   using namespace boost::interprocess;
   //Remove shared memory on construction and destruction
   struct shm_remove
   {
      /*
       *shm_remove() { shared_memory_object::remove("MySharedMemory"); }
       *~shm_remove(){ shared_memory_object::remove("MySharedMemory"); }
       */
   } remover;

   //A managed shared memory where we can construct objects
   //associated with a c-string
   managed_shared_memory segment(open_or_create,
                                 "MySharedMemory",  //segment name
                                 655360000);

   //Alias an STL-like allocator of ints that allocates ints from the segment
   typedef allocator<int, managed_shared_memory::segment_manager>
      ShmemAllocator;

   //Alias a vector that uses the previous STL-like allocator
   typedef vector<int, ShmemAllocator> MyVector;


   const char *rg_name = "OwrVec";
   std::pair<MyVector *, std::size_t> obj = segment.find<MyVector>(rg_name);

   std::cout << "Found Obj" << std::endl;
   std::cout << obj.second << std::endl;
   std::cout << obj.first<< std::endl;
   std::cout << "Found Obj" << std::endl;
   if (obj.first) {
	   for (auto &x : *obj.first) {
		   std::cout << x << std::endl;
		   ++x;
	   }
	   std::cout << "Found Obj" << std::endl;
   }

   pid_t pid;
   int _status;

   return 0;
}
