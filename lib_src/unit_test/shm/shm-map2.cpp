#include "../shm-map.hpp"

#define HASH_NAME "owrHashMap"

int main ()
{
	//srand(time(0));
	//destroy_shm_hashmap(nullptr, HASH_NAME);
	managed_shared_memory *seg;
	seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);
	auto _hash = find_shm_hashmap(seg, HASH_NAME);

	std::cout << _hash.first << std::endl;
	std::cout << _hash.second << std::endl;
	for (auto &x : *_hash.first) {
		std::cout << x.first << std::endl;
		std::cout << x.second.pageno << std::endl;
	}

	//destroy_shm_hashmap(nullptr, HASH_NAME);
	//delete seg;
#if 1
#endif
   /*
    *destroy_shm_seg(SHM_NAME);
    */
   return 0;
}

