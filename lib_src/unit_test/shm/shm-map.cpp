#include "../shm-map.hpp"
#include <sys/wait.h>

#define HASH_NAME "owrHashMap"

int main ()
{
	//srand(time(0));
	destroy_shm_hashmap(nullptr, HASH_NAME);
	managed_shared_memory *seg;

	auto myhashmap = create_shm_hashmap(HASH_NAME);
	PageLock _lock;

	for (int i = 0; i < 10; ++i) {
		myhashmap->insert(ValueType(i, _lock));
		(*myhashmap)[i].pageno = rand() + i;
		pthread_rwlock_init(&((*myhashmap)[i].pg_rwlock), 0);
	}

	for (auto &x : *myhashmap) {
		std::cout << x.first << std::endl;
		std::cout << x.second.pageno << std::endl;
	}

	auto _hash = find_shm_hashmap(seg, HASH_NAME);
	std::cout << _hash.first << std::endl;
	std::cout << _hash.second << std::endl;
	for (auto &x : *_hash.first) {
		std::cout << x.first << std::endl;
		std::cout << x.second.pageno << std::endl;
	}

	int status;
	wait(&status);


	// stay in the memory forever
    /*
	 *destroy_shm_hashmap(nullptr, HASH_NAME);
	 *delete seg;
     */

	return 0;
}

