#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MUTEX "/mutex_lock"
#define MESSAGE "/msg"

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
void test()
{
	/** test **/
	// child
	if (!fork()) {
		sleep(1);
		pthread_mutex_lock(&mtx);
		printf("child get lock\n");
		while (1)
			;
		pthread_mutex_unlock(&mtx);
		printf("child exit\n");
		exit(0);
		// parent
	} else {
		sleep(2);
		pthread_mutex_lock(&mtx);
		printf("parent get lock\n");
		pthread_mutex_unlock(&mtx);

		printf("parent exit\n");
		pthread_mutex_destroy(&mtx);

		int status = 0;
		wait(&status);
	}
}

int main(int argc, char *argv[])
{
	test();
	return 0;
}

