#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <sys/wait.h>
#include <vector>
#include <typeinfo>
//using namespace std;

int main()
{
   using namespace boost::interprocess;
   //Remove shared memory on construction and destruction
   struct shm_remove
   {
      shm_remove() { shared_memory_object::remove("MySharedMemory"); }
      //~shm_remove(){ shared_memory_object::remove("MySharedMemory"); }
   } remover;

   //A managed shared memory where we can construct objects
   //associated with a c-string
   managed_shared_memory segment(open_or_create,
                                 "MySharedMemory",  //segment name
                                 655360000);

   std::cout << sizeof(managed_shared_memory) << std::endl;
   //Alias an STL-like allocator of ints that allocates ints from the segment
   typedef allocator<int, managed_shared_memory::segment_manager>
      ShmemAllocator;
//Alias a vector that uses the previous STL-like allocator
   typedef vector<int, ShmemAllocator> MyVector;

   int initVal[]        = {0, 1, 2, 3, 4, 5, 6 };
   const int *begVal    = initVal;
   const int *endVal    = initVal + sizeof(initVal)/sizeof(initVal[0]);

   //Initialize the STL-like allocator
   const ShmemAllocator alloc_inst (segment.get_segment_manager());

   const char *obj_name = "OwrVec";
   //Construct the vector in the shared memory segment with the STL-like allocator
   //from a range of iterators
   MyVector *myvector =
      segment.construct<MyVector>
         (obj_name)/*object name*/
         (begVal     /*first ctor parameter*/,
         endVal     /*second ctor parameter*/,
         alloc_inst /*third ctor parameter*/);

   managed_shared_memory::handle_t handle = segment.get_handle_from_address(myvector);
   std::cout << segment.get_instance_name(myvector) << std::endl;

   std::pair<MyVector *, std::size_t> obj = segment.find<MyVector>(obj_name);
   /*
    *auto obj1 = segment.find_or_construct<MyVector>(name);
    */

   std::cout << typeid(obj).name() << std::endl;

   std::cout << "Found Obj" << std::endl;
   std::cout << obj.second << std::endl;
   std::cout << obj.first<< std::endl;
   std::cout << "Found Obj" << std::endl;
   if (obj.first) {
	   for (auto &x : *obj.first) {
		   std::cout << x << std::endl;
	   }
	   std::cout << "Found Obj" << std::endl;
   }

   /**
	* to test against the shared memory
	*/
   //std::vector<int> *myvector2 = new std::vector<int>{ 0, 1, 2, 3, 4, 5, 6 };

   pid_t pid;
   int _status;

   if (!fork()) {
	   for (auto &x : *myvector) {
		   std::cout << x << std::endl;
		   ++x;
	   }
	   for (int i = 0; i < 10; ++i)
		   myvector->push_back(i);
	   exit(0);
   } else {

   }

   wait(&_status);

   for (auto &x : *myvector)
	   std::cout << x << std::endl;

   std::cout << &handle << std::endl;
   std::cout << myvector << std::endl;
   std::cout << initVal << std::endl;

   //Use vector as your want
   std::sort(myvector->rbegin(), myvector->rend());
   // . . .
   //When done, destroy and delete vector from the segment
   segment.destroy<MyVector>(obj_name);
   return 0;
}
