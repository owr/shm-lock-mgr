#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MUTEX "/mutex_lock"

void test()
{
	pthread_mutex_t *mtx;
	char *msg;
	int des_msg, des_mtx;
	int mode = S_IRWXU | S_IRWXG;

	des_mtx = shm_open(MUTEX, O_CREAT | O_RDWR | O_TRUNC, mode);
	if (des_mtx < 0) {
		perror("shm_open failed");
		exit(-1);
	}

	if (ftruncate(des_mtx, sizeof(pthread_mutex_t)) == -1) {
		perror("ftruncate failed");
		exit(-1);
	}

	mtx = (pthread_mutex_t*) mmap(NULL, sizeof(pthread_mutex_t),
								  PROT_READ | PROT_WRITE, MAP_SHARED,
								  des_mtx, 0);
	if (mtx == MAP_FAILED) {
		perror("map failed");
		exit(-1);
	}

	/***mutex***/
	pthread_mutexattr_t mtxattr;
	//pthread_mutexattr_settype(&mtxattr, PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutexattr_setpshared(&mtxattr, PTHREAD_PROCESS_SHARED);
	pthread_mutex_init(mtx, &mtxattr);

	/** test **/
	printf("shm-test7 start to get lock\n");
	pthread_mutex_lock(mtx);
	printf("shm-test7 got lock\n");
	while (1)
		;
	pthread_mutex_unlock(mtx);
	printf("shm-test7 exit\n");
	shm_unlink(MUTEX);

	exit(0);
}

int main(int argc, char *argv[])
{
	test();
	return 0;
}

