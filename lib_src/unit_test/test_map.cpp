#include <ctime>
#include <ratio>
#include <chrono>
#include "../shm-map.hpp"
#include <boost/thread/shared_mutex.hpp>
#include <thread>
using namespace std;


void SharedWRLockEffect()
{
	const char *hash_map_name = "test_HashMap";

	destroy_shm_hashmap(hash_map_name);

	managed_shared_memory *seg;
	auto myhashmap = create_shm_hashmap(&seg, hash_map_name);
	PageLock _lock;
	pthread_rwlockattr_t rwlock_attr;
	pthread_rwlockattr_setpshared(&rwlock_attr, PTHREAD_PROCESS_SHARED);
	// write has higher priority
	pthread_rwlockattr_setkind_np(&rwlock_attr,
								  PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);
	for (int i = 0; i < 10; ++i) {
		myhashmap->insert(ValueType(i, _lock));
		(*myhashmap)[i].pageno = rand() + i;
		pthread_rwlock_init(&((*myhashmap)[i].pg_rwlock), &rwlock_attr);
	}
	cout << "TTT" << endl;

	sleep(2);
	std::cout << "parent try to get lock on page 0" << std::endl;
	pthread_rwlock_wrlock(&((*myhashmap)[0].pg_rwlock));

	std::cout << "parent got lock on page 0" << std::endl;
	pthread_rwlock_unlock(&((*myhashmap)[0].pg_rwlock));
	std::cout << "parent release lock on page 0" << std::endl;
}

int main(int argc, char *argv[])
{

	SharedWRLockEffect();
	return 0;
}

