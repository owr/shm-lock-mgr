#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include "page_lock.h"


void multi_process_test()
{
	pid_t pid;

	if ((pid = fork()) == 0) {
		// child
		lock_mgr_init();
		// OK
		//usleep(1000);
		mgr_wpage_lock(43);
		mgr_wpage_unlock(43);

		mgr_wpage_lock(43);
        /*
		 *usleep(1000);
         */
		mgr_wpage_unlock(43);
		exit(0);
	} else {
		// parent
		lock_mgr_init();

		mgr_wpage_lock(43);
		int ret = mgr_wpage_lock(43);
		printf("ret_: %d\n", ret);
		if (!ret)
			mgr_wpage_unlock(43);

		mgr_wpage_lock(43);
		mgr_wpage_unlock(43);
	}

	int status;
	wait(&status);

}

int i = 0;
void *func(void *ptr)
{
	lock_mgr_init();
	mgr_wpage_lock(323);
	int ret = mgr_wpage_lock(323);
	if (!ret)
		mgr_wpage_unlock(323);

	mgr_wpage_unlock(323);
#if 0
	mgr_rpage_lock(323);
	mgr_rpage_unlock(323);

	mgr_wpage_lock(323);
	mgr_wpage_unlock(323);
#endif
}

#define NR_THREADS 10
void multi_thread_test()
{
#if 1
	pthread_t tids[NR_THREADS];
	int s;

	for (int i = 0; i < NR_THREADS; ++i) {
		s = pthread_create(&tids[i], NULL, func, NULL);
		if (s != 0) {
			perror("thread create error");
			exit(-1);
		}
	}

	for (int i = 0; i < NR_THREADS; ++i) {
		s = pthread_join(tids[i], NULL);
		if(s)
			perror("pthread_join");
	}
#endif
}


int main(int argc, char *argv[])
{

	lock_mgr_destroy();

	lock_mgr_init();
	mgr_wpage_lock(323);
	printf("tid: %ld, ================\n\n", gettid());
	mgr_wpage_unlock(323);

	multi_thread_test();
	multi_process_test();

	int ret = mgr_rpage_lock(100);
	int ret2 = mgr_rpage_lock(100);

	printf("ret: %d, ret2: %d\n", ret, ret2);
#if 0
    /*
	 *mgr_rpage_lock(3);
	 *mgr_rpage_unlock(3);
     */

    /*
	 *mgr_insert_page(3280);
     */
	mgr_rpage_lock(3280);
	mgr_rpage_lock(3280);
	mgr_rpage_unlock(3280);
	mgr_rpage_unlock(3280);

	mgr_wpage_lock(3280);
	mgr_wpage_lock(3280);

	mgr_rpage_lock(3280);

    /*
	 *mgr_wpage_lock(3280);
     */

    /*
	 *mgr_wpage_unlock(3280);
     */
#endif
	lock_mgr_destroy();

	return 0;
}
