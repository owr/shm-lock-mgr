#include "../api.h"
#include "../shm-lockmap.hpp"
#include "../shm-deadlock.hpp"
#include "../deadlock_graph.hpp"

#include <string.h>
#include <stdlib.h>
#include <wait.h>
#include <iostream>
#include <unordered_map>
#include <stdio.h>
#include <wait.h>
#include <glog/logging.h>
#include <boost/interprocess/containers/vector.hpp>
using namespace std;
void deadlock_graph_test1()
{
	lock_mgr_destroy();
	lock_mgr_init();
	lock_mgr_destroy();
	int status;
	wait(&status);
}

void deadlock_graph_test2()
{
	lock_mgr_destroy();
	lock_mgr_init();
	auto g = create_shm_dgraph(SHARED_DGRAPH);
	ADJList *adj_set = nullptr;
	int ID_name = 909090;
	adj_set = gbl_mem_seg->template construct<ADJList>
		(std::to_string(ID_name).c_str())(gbl_mem_seg->get_segment_manager());

	(*adj_set).insert(332);
	adj_set->insert(43);
	for (auto &x : *adj_set) {
		cout << x << endl;
	}
	cout << adj_set->size() << endl;
	cout << g->size() << endl;
	auto adj_set2 = *adj_set;
	g->insert({8989, std::move(*adj_set)});
	cout << adj_set << endl;
	g->insert({38989, std::move(adj_set2)});
	cout << g->size() << endl;
	_dump_dgraph(*g);


	lock_mgr_destroy();
}

// for edge test
void deadlock_graph_test3()
{
	lock_mgr_destroy();
	lock_mgr_init();
	dgraph_add_adge(3, 5);
	dgraph_add_adge(13, 15);
	dump_dgraph();

}

// large scale test
void deadlock_graph_test4()
{
	lock_mgr_destroy();
	lock_mgr_init();
	dgraph_add_adge(3, 5);
	dgraph_add_adge(3, 8);
	dgraph_add_adge(5, 3);
	dgraph_add_adge(5, 5);
	dgraph_add_adge(5, 5);
	dgraph_add_adge(5, 5);

	for (int i = 0; i < 1000; ++i)
		dgraph_add_adge((rand() % 129) + 1, (rand() % 129) + 1);
	auto g = *gbl_dlock_graph;
	dump_dgraph();
}

// multiproc test
void deadlock_graph_test5()
{
	lock_mgr_destroy();
	lock_mgr_init();
	dgraph_add_adge(3, 5);
	dgraph_add_adge(3, 8);
	dgraph_add_adge(5, 3);
	dgraph_add_adge(5, 5);
	dump_dgraph();
	LOG(INFO) << gbl_dlock_graph;

	if (fork() == 0) {
		gbl_dlock_graph = nullptr;
		LOG(INFO) << gbl_dlock_graph;
		lock_mgr_init();
		LOG(INFO) << gbl_dlock_graph;
		sleep(1);
		dump_dgraph();
		sleep(2);
		dgraph_add_adge(9999, 8888);
		dump_dgraph();
		exit(0);
	} else {

	}

	// expect to see 8888 here
	dump_dgraph();

	if (fork() == 0) {
		dump_dgraph();
		lock_mgr_init();
		dgraph_add_adge(1111, 2222);
		exit(0);
	}

	int status;
	while (true) {
		int status;
		pid_t done = wait(&status);
		if (done == -1) {
			if (errno == ECHILD) break; // no more child processes
		} else {
			if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
				cerr << "pid " << done << " failed" << endl;
				exit(1);
			}
		}
	}
	dump_dgraph();
}

void deadlock_graph_test6()
{
	lock_mgr_destroy();
	lock_mgr_init();

	//std::unordered_map<int, int> edges = {{1, 2}, {3, 2}, {6, 4}, {5, 4}, {1, 4}, {6, 2}, {2, 3}, {2, 1}, {4, 2}, {5, 3}, {0, 1}, {2, 0}, {1, 5}, {4, 3}, {1, 6}, {4, 1}};
	std::unordered_map<int, int> edges = {
		{3, 5},
		{4, 6},
		{5, 7},
		//{7, 3},
	};

	for (auto &x : edges) {
		dgraph_add_adge(x.first, x.second);
	}

	dump_dgraph();
	//cout << lead_to_circle(2, 3) << endl;
	cout << lead_to_circle(3, 3) << endl;
	cout << lead_to_circle(3, 4) << endl;
	cout << lead_to_circle(3, 6) << endl;
	cout << lead_to_circle(3, 7) << endl;
}

void deadlock_graph_test7()
{
	lock_mgr_destroy();
	lock_mgr_init();

	std::unordered_map<int, int> edges = {
		{3, 5},
		{4, 6},
		{5, 7},
		//{7, 3},
	};

	for (auto &x : edges) {
		dgraph_add_adge(x.first, x.second);
	}

	dump_dgraph();
	flip_edge(4, 6);
	dump_dgraph();
}

void deadlock_graph_test8()
{
	lock_mgr_destroy();
	lock_mgr_init();

	std::unordered_multimap<int, int> edges = {
		{-3, 5},
		{-3, 11},
		{-3, 32},
		{4, 6},
		{4, 8},
		{5, 7},
		{7, 3},
		{8, 9},
		{9, 7},
	};

	for (auto &x : edges) {
		dgraph_add_adge(x.first, x.second);
	}

	dump_dgraph();
	if (fork() == 0) {
		lock_mgr_init();
		remove_edge(-3, 5);
		dump_dgraph();
		remove_edges(-3);
		dump_dgraph();
		remove_edges(4);
		dump_dgraph();
		exit(0);
	}
	int status;
	wait(&status);
	dump_dgraph();
}

/** test for empty graph **/
void deadlock_graph_test9()
{
	lock_mgr_destroy();
	lock_mgr_init();
	dump_dgraph();
	remove_edge(-3, 5);
	dump_dgraph();
	remove_edges(-3);
	dump_dgraph();
	remove_edges(4);
	dump_dgraph();
}

void deadlock_graph_test10()
{
	lock_mgr_destroy();
	lock_mgr_init();
	dump_dgraph();
    /*
	 *std::unordered_multimap<int, int> edges = {
	 *    {-3, 11},
	 *    {-3, 13},
	 *    {12, -3},
	 *    {-4, 12},
	 *};
     */

	LOG(INFO) << "circle: " << lead_to_circle(-4, 11);

	std::unordered_multimap<int, int> edges = {
		// holding
		{-3, 11},
		{-3, 13},
		{-4, 12},
		// to acquire
		{12, -3},
		//{32, -8},
	};

	for (auto &x : edges) {
		dgraph_add_adge(x.first, x.second);
	}
	dump_dgraph();
	//LOG(INFO) << lead_to_circle(-8, 11);
	//reverse graph -4->11
	LOG(INFO) << "circle: " << lead_to_circle(-4, 11);
	// release lock 12 by txn_id 4
	remove_edge(-4, 12);
	dump_dgraph();
	//remove_edge(12, -3);
	// safe here
	dump_dgraph();
	LOG(INFO) << "circle: " << lead_to_circle(-4, 11);
	dump_dgraph();
	flip_edge(12, -3);
	dump_dgraph();
	remove_edges(-3);
	dump_dgraph();
}

int main(int argc, char *argv[])
{
    /*
	 *deadlock_graph_test1();
	 *deadlock_graph_test2();
     */
    /*
	 *deadlock_graph_test3();
     */
    /*
	 *deadlock_graph_test3();
     */
    /*
	 *deadlock_graph_test4();
	 *deadlock_graph_test5();
     */
	//deadlock_graph_test6();
	//deadlock_graph_test7();
	//deadlock_graph_test8();
	//deadlock_graph_test9();
	deadlock_graph_test10();
	return 0;
}


