#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <wait.h>
#include "../page_lock.h"

int main(int argc, char *argv[])
{
	lock_mgr_destroy();

	lock_mgr_init();

	lock_mgr_destroy();

	return 0;
}
