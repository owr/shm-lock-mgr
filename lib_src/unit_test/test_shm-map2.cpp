#include <gtest/gtest.h>
#include <ctime>
#include <ratio>
#include <chrono>
#include "../shm-map.hpp"
#include <boost/thread/shared_mutex.hpp>
#include <thread>
using namespace std;

// there sshould not be lock contention here, becasuse child and parent use different memory
TEST(NoSharedLockTest, DISABLED_NoSharedLockEffect)
{
	pthread_rwlock_t rwlock_no_share;
	pthread_rwlock_init(&rwlock_no_share, 0);
	if (!fork()) {
		std::cout << "child try to get lock on page 0" << std::endl;
		pthread_rwlock_wrlock(&rwlock_no_share);
		std::cout << "child got lock on page 0" << std::endl;
		std::cout << "child sleep 1 sec" << std::endl;
		sleep(1);
		pthread_rwlock_unlock(&rwlock_no_share);
		std::cout << "child released lock on page 0" << std::endl;
		exit(0);
	} else {
		usleep(100000);
		std::cout << "parent try to get lock on page 0" << std::endl;
		pthread_rwlock_wrlock(&rwlock_no_share);

		std::cout << "parent got lock on page 0" << std::endl;
		pthread_rwlock_unlock(&rwlock_no_share);
		std::cout << "parent release lock on page 0" << std::endl;

		int status;
		wait(&status);
	}
}

TEST(MutexTest, DISABLED_Mutex)
{
	const char *hash_map_name = "test_HashMap";

	destroy_shm_hashmap(hash_map_name);

	managed_shared_memory *seg;
	auto myhashmap = create_shm_hashmap(&seg, hash_map_name);
	PageLock _lock;

	for (int i = 0; i < 10; ++i) {
		myhashmap->insert(ValueType(i, _lock));
		(*myhashmap)[i].pageno = rand() + i;
		//pthread_mutex_init(&((*myhashmap)[i].mtx), 0);
	}

	if (!fork()) {
		std::cout << "child try to get lock on page 0" << std::endl;
		//pthread_mutex_lock(&((*myhashmap)[0].mtx));
		std::cout << "child got lock on page 0" << std::endl;
		std::cout << "child sleep 1 sec" << std::endl;
		sleep(1);

		//pthread_mutex_unlock(&((*myhashmap)[0].mtx));
		std::cout << "child released lock on page 0" << std::endl;
		exit(0);
	} else {
		usleep(100000);
		std::cout << "parent try to get lock on page 0" << std::endl;
		//pthread_mutex_lock(&((*myhashmap)[0].mtx));

		std::cout << "parent got lock on page 0" << std::endl;
		//pthread_mutex_unlock(&((*myhashmap)[0].mtx));
		std::cout << "parent release lock on page 0" << std::endl;

		int status;
		wait(&status);
		destroy_shm_hashmap(hash_map_name);
		delete seg;
	}
}


// test shared read lock, there should be no contention here becasuse rw lock support multi-readers
TEST(SharedRDLockTest, DISABLED_SharedRDLockEffect)
{
	const char *hash_map_name = "test_HashMap";

	destroy_shm_hashmap(hash_map_name);

	managed_shared_memory *seg;
	auto myhashmap = create_shm_hashmap(&seg, hash_map_name);
	PageLock _lock;

	for (int i = 0; i < 10; ++i) {
		myhashmap->insert(ValueType(i, _lock));
		(*myhashmap)[i].pageno = rand() + i;
		pthread_rwlock_init(&((*myhashmap)[i].pg_rwlock), 0);
	}

	if (!fork()) {
		std::cout << "child try to get lock on page 0" << std::endl;
		pthread_rwlock_rdlock(&((*myhashmap)[0].pg_rwlock));
		std::cout << "child got lock on page 0" << std::endl;

		std::cout << "child sleep 1 sec" << std::endl;
		sleep(1);

		pthread_rwlock_unlock(&((*myhashmap)[0].pg_rwlock));
		std::cout << "child released lock on page 0" << std::endl;
		exit(0);
	} else {
		usleep(100000);
		std::cout << "parent try to get lock on page 0" << std::endl;
		pthread_rwlock_rdlock(&((*myhashmap)[0].pg_rwlock));

		std::cout << "parent got lock on page 0" << std::endl;
		pthread_rwlock_unlock(&((*myhashmap)[0].pg_rwlock));
		std::cout << "parent release lock on page 0" << std::endl;

		int status;
		wait(&status);
		destroy_shm_hashmap(hash_map_name);
		delete seg;
	}
}

// FIXME test shared write lock, there should be contention here
TEST(SharedWRLockTest, SharedWRLockEffect)
{
	const char *hash_map_name = "test_HashMap";

	destroy_shm_hashmap(hash_map_name);

	managed_shared_memory *seg;
	cout << "TTT" << endl;

	seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);
	auto _hash = find_shm_hashmap(seg, hash_map_name);

	auto _myhashmap = _hash.first;
	cout << "hash: " << _hash.first << endl;
	cout << "hashsec: " << _hash.second << endl;
	std::cout << "child try to get lock on page 0" << std::endl;
	pthread_rwlock_wrlock(&((*_myhashmap)[0].pg_rwlock));
	std::cout << "child got lock on page 0" << std::endl;

	std::cout << "child sleep 1 sec" << std::endl;
	// FIXME, problems here, we need to use pthread_cond_t to wake parent up
	usleep(3);
	pthread_rwlock_unlock(&((*_myhashmap)[0].pg_rwlock));

	// wakeup
	std::cout << "child released lock on page 0" << std::endl;
	exit(0);

    /*
	 *destroy_shm_hashmap(nullptr, hash_map_name);
	 *delete seg;
     */
}

// C++11 mutex test;
TEST(SharedMutexTest, TestSharedMutex)
{
}



// test shared read/write lock, there should be contention here
TEST(SharedRWLockTest, DISABLED_SharedRWLockEffect)
{
	const char *hash_map_name = "test_HashMap";

	destroy_shm_hashmap(hash_map_name);

	managed_shared_memory *seg;
	auto myhashmap = create_shm_hashmap(&seg, hash_map_name);
	PageLock _lock;

	for (int i = 0; i < 10; ++i) {
		myhashmap->insert(ValueType(i, _lock));
		(*myhashmap)[i].pageno = rand() + i;
		pthread_rwlock_init(&((*myhashmap)[i].pg_rwlock), 0);
	}

	if (!fork()) {
		std::cout << "child try to get read lock on page 0" << std::endl;
		pthread_rwlock_rdlock(&((*myhashmap)[0].pg_rwlock));
		std::cout << "child got read lock on page 0" << std::endl;

		std::cout << "child sleep 1 sec" << std::endl;
		// FIXME, problems here, we need to use pthread_cond_t to wake parent up
		//usleep(100000);

		pthread_rwlock_unlock(&((*myhashmap)[0].pg_rwlock));
		std::cout << "child released read lock on page 0" << std::endl;
		exit(0);
	} else {
		usleep(100000);
		std::cout << "parent try to get write lock on page 0" << std::endl;
		pthread_rwlock_wrlock(&((*myhashmap)[0].pg_rwlock));

		std::cout << "parent got write lock on page 0" << std::endl;
		pthread_rwlock_unlock(&((*myhashmap)[0].pg_rwlock));
		std::cout << "parent release write lock on page 0" << std::endl;

		int status;
		wait(&status);
		destroy_shm_hashmap(hash_map_name);
		delete seg;
	}
}

TEST(BenchTest, DISABLED_Benchmark)
{
	const char *hash_map_name = "test_HashMap";

	destroy_shm_hashmap(hash_map_name);

	managed_shared_memory *seg;
	auto myhashmap = create_shm_hashmap(&seg, hash_map_name);
	PageLock _lock;

	using namespace std;
	using namespace std::chrono;
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (int i = 0; i < 1000000; ++i) {
		myhashmap->insert(ValueType(i, _lock));
        /*
		 *(*myhashmap)[i].pageno = rand() + i;
		 *pthread_rwlock_init(&((*myhashmap)[i].pg_rwlock), 0);
         */
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << time_span.count() << " seconds." << endl;
	std::cout << myhashmap->size() << std::endl;

	int idx = rand() % 1000000;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < 100; ++i) {
		idx = rand() % 1000000;
		cout << &(*myhashmap)[idx] << endl;

	}
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << time_span.count() << " seconds." << endl;

}



int main(int argc, char *argv[])
{
	testing::InitGoogleTest(&argc, argv);
	auto ret = RUN_ALL_TESTS();
	int status;
	wait(&status);
	return ret;
}

