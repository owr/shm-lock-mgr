#include <gtest/gtest.h>
#include <ctime>
#include <ratio>
#include <chrono>
#include <boost/thread/shared_mutex.hpp>
#include <thread>
#include "../page_lock.h"
#include "../common.h"
using namespace std;

/**
 * reader fist get lock, then sleep,
 * writer try to get lock
 */
static void do_lock_test_write(int sleep_time, int usleep_time)
{
	auto _lock = init_shared_rwlock(SHARED_RWLOCK);
	cout << _lock << endl;
	usleep(usleep_time);
	std::cout << "child try to get lock on page 1" << std::endl;
	pthread_rwlock_rdlock(_lock);
	std::cout << "child got lock on page 1" << std::endl;

	std::cout << "child sleep 1 sec" << std::endl;
	sleep(sleep_time);

	pthread_rwlock_unlock(_lock);
	std::cout << "child released lock on page 1" << std::endl;
}

TEST(Lock_RWLockTest, DISABLED_Lock_TestRWLock)
{
	destroy_shared_rwlock(SHARED_RWLOCK);
	auto lock_ = init_shared_rwlock(SHARED_RWLOCK);
	assert(lock_);

	if (!fork()) {
		do_lock_test_write(1, 0);
		exit(0);
	} else {
		do_lock_test_write(0, 100000);
		int status;
		wait(&status);
	}
}

TEST(LockMgrTest, DISABLED_TestLockMgr)
{
	lock_mgr_destroy();
	lock_mgr_init();

	if (!fork()) {
		do_lock_test_write(1, 0);
		exit(0);
	} else {
		do_lock_test_write(0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}

TEST(InitTest, DISABLED_TestInit)
{
	lock_mgr_destroy();
	lock_mgr_init();
	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);

	lock_mgr_destroy();
	//EXPECT_EQ(gbl_lock_mgr, nullptr);
}

TEST(RDLockTest, DISABLED_TestRDLock)
{

	lock_mgr_destroy();
	lock_mgr_init();
	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	if (!fork()) {
		lock_mgr_init();
		do_read_page(1, 1, 0);
		lock_mgr_destroy();
		exit(0);
	} else {
		do_read_page(1, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}


TEST(WRLockTest, DISABLED_TestWRLock)
{
	lock_mgr_destroy();
	lock_mgr_init();
	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	if (!fork()) {
		do_write_page(1, 1, 0);
		exit(0);
	} else {
		do_write_page(1, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}


TEST(RWLockTest, DISABLED_TestRWLock)
{
	lock_mgr_destroy();
	lock_mgr_init();
	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	if (!fork()) {
		do_read_page(1, 1, 0);
		exit(0);
	} else {
		do_write_page(1, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}

TEST(RWMixedLockTest, DISABLED_TestRWLockMixed)
{
	lock_mgr_destroy();
	lock_mgr_init();
	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	if (!fork()) {
		do_read_page(1, 1, 0);
		exit(0);
	} else {
		do_write_page(2, 0, 100000);
		int status;
		wait(&status);
	}
	lock_mgr_destroy();
}


TEST(ThreadRWLockTest, DISABLED_TestRWLockThread)
{
	lock_mgr_destroy();
	lock_mgr_init();
	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);
	mgr_insert_page(1);
	mgr_insert_page(3);
	mgr_insert_page(5);
	dump_mrg();

	thread thds[2];
	thds[0] = thread(do_read_page, 1, 1, 0);
	thds[1] = thread(do_write_page, 1, 0, 100000);
	thds[0].join();
	thds[1].join();
}


TEST(BenchTest, TestBench)
{
	using namespace std;
	using namespace std::chrono;
	duration<double> time_span;
	high_resolution_clock::time_point t1, t2;

	t1 = high_resolution_clock::now();
	lock_mgr_destroy();
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);
	cout << time_span.count() << " seconds." << endl;


	t1 = high_resolution_clock::now();
	lock_mgr_init();
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);
	cout << time_span.count() << " seconds." << endl;

	EXPECT_NE(gbl_mgr_rwlock, nullptr);
	EXPECT_NE(gbl_lock_mgr, nullptr);

//#define N_PG 5000000
#define N_PG 5000000
	int pg = N_PG;

t1 = high_resolution_clock::now();
	while (pg--)
		mgr_insert_page(pg+1);
t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);
	cout << "insert_: " << time_span.count() << " seconds." << endl;
	cout << static_cast<UODR_HashMap*>(gbl_lock_mgr)->size() << endl;

	int idx = rand() % N_PG;
	int j = 0;
t1 = high_resolution_clock::now();
	for (int i = 0; i < N_PG; ++i) {
		idx = rand() % N_PG;
		mgr_rpage_lock(idx);
		++j;
		mgr_rpage_unlock(idx);
	}
t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << "lock_: " << time_span.count() << " seconds." << endl;
	cout << j << endl;

	//dump_mrg();
	t1 = high_resolution_clock::now();
	lock_mgr_destroy();
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << time_span.count() << " seconds." << endl;
}


int main(int argc, char *argv[])
{
	testing::InitGoogleTest(&argc, argv);
	auto ret = RUN_ALL_TESTS();
	int status;
	wait(&status);
	return ret;
}

