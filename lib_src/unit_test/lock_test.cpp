#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include "page_lock.h"


void multi_process_test()
{
	pid_t pid;

	if ((pid = fork()) == 0) {
		// child
		lock_mgr_init();
        /*
		 *sleep(1);
         */
		mgr_rpage_lock(43);
		mgr_rpage_unlock(43);
		mgr_rpage_lock(43);
		mgr_rpage_unlock(43);
		exit(0);
        /*
		 *mgr_wpage_unlock(43);
         */
	} else {
		// parent
		lock_mgr_init();

		mgr_rpage_lock(43);
		mgr_rpage_unlock(43);
		mgr_rpage_lock(43);
		mgr_rpage_unlock(43);
		exit(0);
	}

	int status;
	wait(&status);

}

int i = 0;
void *func(void *ptr)
{
	lock_mgr_init();
	mgr_wpage_lock(323);
	//usleep(1000);
	mgr_wpage_unlock(323);
}

#define NR_THREADS 1
void multi_thread_test()
{
#if 1
	pthread_t tids[NR_THREADS];
	int s;

	for (int i = 0; i < NR_THREADS; ++i) {
		s = pthread_create(&tids[i], NULL, func, NULL);
		if (s != 0) {
			perror("thread create error");
			exit(-1);
		}
	}

	for (int i = 0; i < NR_THREADS; ++i) {
		s = pthread_join(tids[i], NULL);
		if(s)
			perror("pthread_join");
	}
#endif
}


int main(int argc, char *argv[])
{
	lock_mgr_destroy();
	lock_mgr_init();

	printf("tid: %ld, ================\n\n", gettid());

	//multi_thread_test();
	multi_process_test();

#if 0
    /*
	 *mgr_rpage_lock(3);
	 *mgr_rpage_unlock(3);
     */

    /*
	 *mgr_insert_page(3280);
     */
	mgr_rpage_lock(3280);
	mgr_rpage_lock(3280);
	mgr_rpage_unlock(3280);
	mgr_rpage_unlock(3280);

	mgr_wpage_lock(3280);
	mgr_wpage_lock(3280);

	mgr_rpage_lock(3280);

    /*
	 *mgr_wpage_lock(3280);
     */

    /*
	 *mgr_wpage_unlock(3280);
     */
#endif
	lock_mgr_destroy();

	return 0;
}
