#include "../api.h"
#include "../shm-lockmap.hpp"
#include "../deadlock_graph.hpp"


#include <string.h>
#include <iostream>
#include <stdio.h>
#include <wait.h>
#include <glog/logging.h>
#include <boost/interprocess/containers/vector.hpp>
using namespace std;

struct mystruct {
	int key;
	char attr[10];
};

void hash_test1()
{

	struct mystruct mm[3];
	mm[0].key = 0;
	strcpy(mm[0].attr, "zero");
	mm[1].key = 1;
	strcpy(mm[1].attr, "first");
	mm[2].key = 2;
	strcpy(mm[2].attr, "second");

	void *hash_tbl = hashtbl_create();
	hashtbl_add(hash_tbl, 0, &mm[0]);
	hashtbl_add(hash_tbl, 1, &mm[1]);
	hashtbl_add(hash_tbl, 2, &mm[2]);

	struct mystruct *_f = (struct mystruct*)hashtbl_find(hash_tbl, 1);
	printf("%s\n", _f->attr);
	_f = (struct mystruct*)hashtbl_find(hash_tbl, 10);

	printf("%p\n", _f);

}

// refer to libs/interprocess/test/vector_test.hpp
// should always be fine
void txn_lock_list_test1()
{
	lock_mgr_destroy();
	lock_mgr_init();
	lock_mgr_destroy();
	int status;
	wait(&status);
}


void txn_lock_list_test2()
{
	lock_mgr_destroy();
	lock_mgr_init();
	add_lock_to_txn(3, 43);
	add_lock_to_txn(3, 65);
	add_lock_to_txn(3, 890);
	dump_locks_on_txn(3);

	//lock_mgr_destroy();

}


void txn_lock_list_test3()
{
	lock_mgr_destroy();
	lock_mgr_init();

	txn_lock_acquire(3, 43);
	txn_lock_acquire(3, 43);
	dump_locks_on_txn(3);

	txn_lock_release(3, 43);
	dump_locks_on_txn(3);

	txn_lock_acquire(3, 65);
	dump_locks_on_txn(3);
	txn_lock_release(3, 65);
	dump_locks_on_txn(3);

	txn_lock_acquire(3, 11);
	txn_lock_acquire(3, 12);
	txn_lock_acquire(3, 13);
	txn_lock_acquire(3, 14);
	txn_lock_acquire(3, 15);
	dump_locks_on_txn(3);
	txn_lock_release_all(3);
	dump_locks_on_txn(3);
	dump_dgraph();
}

// test proc
void txn_lock_list_test4()
{
	lock_mgr_destroy();
	lock_mgr_init();
	txn_lock_acquire(gettid(), 43);
	txn_lock_acquire(gettid(), 65);
	//txn_lock_acquire(3, 65);
	dump_dgraph();
	dump_locks_on_txn(gettid());
	txn_lock_release_all(gettid());
	exit(0);

	txn_lock_acquire(3, 11);
	txn_lock_acquire(3, 11);
	txn_lock_acquire(3, 12);
	txn_lock_acquire(3, 13);
	txn_lock_acquire(3, 14);
	txn_lock_acquire(3, 15);
	dump_locks_on_txn(3);
	txn_lock_release_all(3);
	dump_locks_on_txn(3);
}

// multiproc test
void txn_lock_list_test5()
{
	lock_mgr_destroy();
	lock_mgr_init();
	auto txn_id = gettid();
	int ret;
	ret = txn_lock_acquire(txn_id, 43);
	assert(ret == 0);
	ret = txn_lock_acquire(txn_id, 43);
	assert(ret == 0);
	ret = txn_lock_acquire(txn_id, 65);
	assert(ret == 0);
	ret = txn_lock_acquire(txn_id, 65);
	assert(ret == 0);
	dump_locks_on_txn(txn_id);

#if 1
	if (fork() == 0) {
		// child
		lock_mgr_init();
		txn_id = gettid();
		ret = txn_lock_acquire(txn_id, 43);
		assert(ret == 0);
		ret = txn_lock_acquire(txn_id, 43);
		assert(ret == 0);
		ret = txn_lock_acquire(txn_id, 65);
		assert(ret == 0);
		ret = txn_lock_acquire(txn_id, 65);
		assert(ret == 0);
		dump_locks_on_txn(txn_id);
		txn_lock_release_all(txn_id);
	} else {
		sleep(1);
		txn_lock_release_all(txn_id);
	}
#endif
	int status;
	wait(&status);
}

void txn_lock_list_test6()
{
	lock_mgr_destroy();
	lock_mgr_init();
	auto txn_id = gettid();
	//txn_lock_acquire(txn_id, 43);
	txn_lock_acquire(43, 43);
	//txn_lock_acquire(43, 43);
    /*
	 *txn_lock_acquire(txn_id, 43);
	 *txn_lock_acquire(txn_id, 65);
	 *txn_lock_acquire(txn_id, 65);
	 *dump_locks_on_txn(txn_id);
     */
	//txn_lock_release_all(txn_id);
	//txn_lock_release_all(txn_id);
}

// deadlock test
void txn_lock_list_test7()
{
	lock_mgr_destroy();
	lock_mgr_init();
	auto txn_id = gettid();
	int ret;
	ret = txn_lock_acquire(txn_id, 43);
	assert(ret == 0);

	if (fork() == 0) {
		// child
		lock_mgr_init();
		txn_id = gettid();

		ret = txn_lock_acquire(txn_id, 65);
		assert(ret == 0);
		dump_locks_on_txn(txn_id);
		sleep(2);
		LOG(INFO) << "deadlock ";
		dump_dgraph();

		ret = txn_lock_acquire(txn_id, 43);
		assert(ret == DDLOCK);
		txn_lock_release_all(txn_id);
	} else {
		sleep(1);
		ret = txn_lock_acquire(txn_id, 65);
		assert(ret != DDLOCK);
		dump_locks_on_txn(txn_id);

		sleep(1);
		txn_lock_release_all(txn_id);
	}
	int status;
	wait(&status);
	dump_dgraph();
}

void txn_lock_list_test8()
{
	lock_mgr_destroy();
	lock_mgr_init();
	auto txn_id = gettid();
	int ret;
	ret = txn_lock_acquire(txn_id, 43);
	assert(ret == 0);
	ret = txn_lock_acquire(txn_id, 73);
	assert(ret == 0);

	if (fork() == 0) {
		if (fork() == 0) {
			lock_mgr_init();
			auto txn_id = gettid();
			ret = txn_lock_acquire(txn_id, 99);
			assert(ret == 0);
			txn_lock_release_all(txn_id);
			exit(0);
		}

		// child
		lock_mgr_init();
		txn_id = gettid();

		sleep(1);
		ret = txn_lock_acquire(txn_id, 65);
		assert(ret == 0);
		ret = txn_lock_acquire(txn_id, 99);
		assert(ret == 0);
		ret = txn_lock_acquire(txn_id, 73);
		assert(ret == 0);
		dump_locks_on_txn(txn_id);
		sleep(2);
		LOG(INFO) << "deadlock ";
		dump_dgraph();

		ret = txn_lock_acquire(txn_id, 43);
		assert(ret == 0);
		//if (ret != 0)
		txn_lock_release_all(txn_id);
	} else {
		sleep(2);
		ret = txn_lock_acquire(txn_id, 65);
		assert(ret != 0);
		dump_locks_on_txn(txn_id);

		sleep(1);
		txn_lock_release_all(txn_id);
	}
	int status;
    /*
	 *wait(&status);
	 *wait(&status);
     */
	dump_dgraph();
	sleep(3);
}



void page_lock_test()
{
	lock_mgr_destroy();
	lock_mgr_init();
	mgr_enter_mutex(43);
	mgr_leave_mutex(43);
	cout << mgr_search_page(43)->ref_count << endl;;

	lock_mgr_destroy();
}


int main(int argc, char *argv[])
{
	//google::InitGoogleLogging(argv[0]);
	LOG(INFO) << "Found " << 32 << " cookies";

	printf("%ld\n", int_concat(323232132322, 898));
	hash_test1();
	op();
	//page_lock_test();

	//txn_lock_list_test1();
	//txn_lock_list_test2();
	//txn_lock_list_test3();
	//txn_lock_list_test4();
	//txn_lock_list_test5();
	txn_lock_list_test6();
	//txn_lock_list_test5();
	//txn_lock_list_test7();
	//txn_lock_list_test8();
	return 0;
}


