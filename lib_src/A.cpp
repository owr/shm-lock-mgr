#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include "A.hpp"

using namespace std;

void A::_ordered_map()
{
	map<uint32_t, string> _map;
	for (int i = 0; i < 100; ++i) {
		_map[i] = to_string(i);
	}

    /*
	 *sort(_map.begin(), _map.end(), [&](auto x, auto y) {
	 *     return x > y;
	 *     });
     */

	for (auto &x : _map) {
		cout << x.first << ":";
		cout << x.second<< ", ";
	}
	cout << endl;

}

void A::op()
{
	_ordered_map();
	srand(time(0));
	std::vector<int> v;
	for (int i = 0; i < 10; ++i)
		v.push_back(rand());

	copy(v.begin(), v.end(), ostream_iterator<int>(std::cout, ","));

	for (auto &i : v)
		cout << i << endl;

	cout << "C call C++ library test" << endl;
	cout << __func__ << endl;
}

/*
 *#define DEBUG
 *#ifdef DEBUG
 *int main(int argc, char *argv[])
 *{
 *    A a;
 *    a.op();
 *    return 0;
 *}
 *#endif
 */
