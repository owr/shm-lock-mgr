#ifndef __SHM_DEADLOCK_HPP__
#define __SHM_DEADLOCK_HPP__

#include "shm-obj.hpp"
#include "page_lock.h"
#include <glog/logging.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * For handling deadlock
 * construct shared memory transaction lock list
 */
static DeadlockGraph *create_shm_dgraph(const char *graph_name)
{
	auto _seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);
	DeadlockGraph *dgraph = nullptr;
	auto pr = find_shm_object<DeadlockGraph>(_seg, graph_name);

	// if not exists, then create
#ifdef PGLK_DEBUG
	LOG(INFO) << "tid: " << gettid() << ", pr.first: " << pr.first;
	LOG(INFO) << "tid: " << gettid() << ", graph_name: " << graph_name;
#endif
	if (!pr.first) {
		dgraph = _seg->construct<DeadlockGraph>(graph_name)
			(3, boost::hash<DGKeyType>(), std::equal_to<DGKeyType>()
			 , _seg->get_allocator<DGValueType>());
	} else {
#ifdef PGLK_DEBUG
		LOG(INFO) << "tid: " << gettid() << ", dgraph: " << graph_name
			<< " existed, just return";
#endif
		dgraph = pr.first;
	}

	//gbl_mem_seg = _seg;

	return dgraph;
}

/**
 * destroy shared memory hash map
 */
static void destroy_shm_dgraph(const char *graph_name)
{
	/**
	 * skip the shm object destroy, destroy the whole shared memory
	 * directly, shm object destroy will take a long time because
	 * pointer cast back and forth in boost, ptr_offset
	 */
	destroy_shm_object<DeadlockGraph>(gbl_mem_seg, graph_name);
}

#ifdef __cplusplus
}
#endif


#endif
