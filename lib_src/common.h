#ifndef __COMMON_H__
#define __COMMON_H__

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>     //boost::unordered_map
#include <functional>                  //std::equal_to
#include <boost/functional/hash.hpp>   //boost::hash
#include <atomic>
#include "page_lock.h"

using namespace boost::interprocess;

#ifdef __cplusplus
extern "C" {
#endif

#define HASH_SEG_NAME "owr_UODR_HashMap_SEG"
#define HASH_SEG_MAX_SIZE 409600000

using KeyType = uint32_t;

/*************** types for shared hashmap ***********/
/* For shared lock
 *using MappedType = struct PageLock;
 *using ValueType = std::pair<uint32_t, struct PageLock>;
 */
using MappedType = struct PageLockMtx;
using ValueType = std::pair<uint32_t, struct PageLockMtx>;

//Typedef the allocator
using ShmemAllocator =
allocator<ValueType, managed_shared_memory::segment_manager>;

//Alias an unordered_map of ints that uses the previous STL-like allocator.
using UODR_HashMap =
boost::unordered_map<KeyType, MappedType, boost::hash<KeyType>,
	std::equal_to<KeyType>, ShmemAllocator>;

/*************** types for txn management ***********/
using Pgno = uint32_t;
using TXNId = uint32_t;
// list of pages holding on TXNId
using allocator_int_t = allocator<Pgno, managed_shared_memory::segment_manager>;
using PageVec = boost::interprocess::vector<Pgno, allocator_int_t>;
// can be optimized with unordered_map
using TXNLockMapMappedType = PageVec;
using TXNLockMapValueType = std::pair<TXNId, PageVec>;
using TXNLockMapShmemAllocator =
allocator<TXNLockMapValueType, managed_shared_memory::segment_manager>;

using TXNLockMap = boost::unordered_map<TXNId, TXNLockMapMappedType,
	  boost::hash<TXNId>, std::equal_to<TXNId>, TXNLockMapShmemAllocator>;

/*************** types for shared shm deadlock detector, a graph ***********/
// for graph node
using NodeId = int32_t;
// type for source id
using RSCNodeId = int32_t;
// type for txn id
using TXNNodeId = int32_t;
using DKeyType = NodeId;
// allocator for deadlock node
using allocator_dnode_t = allocator<DKeyType, managed_shared_memory::segment_manager>;
// list of pages holding
using ADJList = boost::unordered_set<DKeyType,
	  boost::hash<DKeyType>, std::equal_to<DKeyType>, allocator_dnode_t>;

// for graph
using DGKeyType = NodeId;
using DGMappedType = ADJList;
using DGValueType = std::pair<DGKeyType, DGMappedType>;
using allocator_dg_t = allocator<DGValueType, managed_shared_memory::segment_manager>;
using DeadlockGraph = boost::unordered_map<DGKeyType, DGMappedType,
	  boost::hash<DGKeyType>, std::equal_to<DGKeyType>, allocator_dg_t>;

/**
 * Global lock map handler
 */
extern TXNLockMap *gbl_txn_lock_map;

/**
 * Global deadlock handler
 */
extern DeadlockGraph *gbl_dlock_graph;
// reverse graph of gbl_dlock_graph
extern DeadlockGraph *gbl_dlock_graph_r;
extern struct ShmRWLock *gbl_dgraph_lck;

/**
 * shared memory segment, for deleting
 */
extern std::shared_ptr<managed_shared_memory> gbl_mem_seg;

/**
 * Page Lock management entity
 */
struct PageLock {
	uint32_t pageno; /* can be optimized out */
	//std::atomic<int32_t> ref_count; /* page ref_count */

	pthread_rwlock_t pg_rwlock;
	int stat; /* reserved for reallocation */
};

struct PageLockMtx {
	uint32_t pageno; /* can be optimized out */

	pthread_mutex_t ref_count_mtx;
	int32_t ref_count; /* page ref_count, protect by ref_count_mtx */

	pthread_mutex_t pg_mutex;
	int stat; /* reserved for reallocation */
};


/**
 * For global shm management, the last one who
 * destroy the lock manager should delete the shm
 * segment
 */
struct ShmRWLock {
	pthread_rwlock_t rw_lock;
	std::atomic<int32_t> ref_count;
};

/**
 * The pointer owned by single process, not shared among processes, since
 * there is no contention here, the real data is stored in shm
 */
extern struct ShmRWLock *gbl_seg_lck;

#ifdef __cplusplus
}
#endif

#endif
