#ifndef __SHM_OBJ_HPP__
#define __SHM_OBJ_HPP__
#include "common.h"

/********************************************************/
/***  A general interface to create shared objects ******/
/********************************************************/

/**
 * Initiate a shared memory segment, this is the holder for
 * all shared objects. Memory segment is the raw memory region
 * where we can allocate shared memory from.
 **/
static std::shared_ptr<managed_shared_memory>
init_shm_seg(const char *seg_name, uint32_t seg_size)
{

	using namespace std;
	if (gbl_mem_seg == nullptr) {
		auto perm = boost::interprocess::permissions();
		perm.set_unrestricted();
		return std::make_shared<managed_shared_memory>(open_or_create, seg_name, seg_size, nullptr, perm);
	} else {
		return gbl_mem_seg;
	}
}

/**
 * Destroy the shared memory segment, this will destroy
 * all objects allocated in the shared segment
 **/
static void destroy_shm_seg(const char *seg_name)
{
	shared_memory_object::remove(seg_name);
}

/**
 * Construct the object instance object identified by obj_name,
 * this function is thread safe
 **/
template<class T>
static T *create_shm_object(const managed_shared_memory *seg,
			  const char *obj_name)
{
	return seg->construct<T>(obj_name);
}

/**
 * destroy the object created by name, thread-safe
 */
template<class T>
static void destroy_shm_object(const std::shared_ptr<managed_shared_memory> &seg,
							   const char *obj_name)
{
	if (seg)
		seg->destroy<T>(obj_name);
}

/**
 * find the object we want from the shared memory segment.
 */
template<class T>
static std::pair<T*, std::size_t>
find_shm_object(const std::shared_ptr<managed_shared_memory> &seg,
					  const char *obj_name)
{
   return seg->find<T>(obj_name);
}


/**
 * Destroy the shared memory segment
 */
static void destroy_seg()
{
	destroy_shm_seg(HASH_SEG_NAME);
	gbl_mem_seg = nullptr;
}

/**
 * We need a **shared** lock to protect the big shared lock manager
 * then we create shared objects atop shm seg
 */
static ShmRWLock *init_shared_rwlock(const char* lock_name)
{
	auto _seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);

	assert(_seg);
	auto pr = find_shm_object<ShmRWLock>(_seg, lock_name);
	ShmRWLock *seglock = nullptr;

	if (!pr.first) {
		seglock = _seg->construct<ShmRWLock>(lock_name)();

		pthread_rwlockattr_t rwlock_attr;
		pthread_rwlockattr_setpshared(&rwlock_attr,
									  PTHREAD_PROCESS_SHARED);
		/* writer has higher priority */
		pthread_rwlockattr_setkind_np(&rwlock_attr,
			  PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);

		pthread_rwlock_init(&seglock->rw_lock, &rwlock_attr);
		seglock->ref_count = 0;

	} else {
		seglock = pr.first;
	}
	gbl_mem_seg = _seg;
	return seglock;
}

static void destroy_shared_rwlock(const char *lock_name)
{
	destroy_shm_object<ShmRWLock>(gbl_mem_seg, lock_name);
}

#endif
