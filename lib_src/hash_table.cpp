#include <unordered_map>
#include <stdio.h>
#include <stdio.h>
#include <stdint.h>
using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

void* hashtbl_create()
{
	return new unordered_map<uint32_t, void *>();
}

void hashtbl_add(void *hash_tbl, uint32_t key, void *val)
{
	auto _hash_tbl = (unordered_map<uint32_t, void*> *)hash_tbl;
	(*_hash_tbl)[key] = val;
}

void *hashtbl_find(void *hash_tbl, uint32_t key)
{
	auto _hash_tbl = (unordered_map<uint32_t, void*> *)hash_tbl;
	return (*_hash_tbl)[key];
}

void hashtbl_del(void *hash_tbl, uint32_t key)
{
	auto _hash_tbl = (unordered_map<uint32_t, void*> *)hash_tbl;
	_hash_tbl->erase(key);
}

void hashtbl_free(void *hash_tbl)
{
	auto _hash_tbl = (unordered_map<uint32_t, void*> *)hash_tbl;
	delete _hash_tbl;
}

void hashtbl_dump(void *hash_tbl)
{
	auto _hash_tbl = (unordered_map<uint32_t, void*> *)hash_tbl;
	for (auto &x : *_hash_tbl)
		printf("%u, ", x.first);
}


#if 0

/*
 *void hash_find
 *
 */
struct ms {
	int key;
	string attr;
};

void hash_test()
{
	struct ms mm[3];
	mm[0].key = 0;
	mm[0].attr = "zero";
	mm[1].key = 1;
	mm[1].attr = "first";
	mm[2].key = 2;
	mm[2].attr = "second";

	auto _hash = (unordered_map<uint32_t, ms*> *)hashtbl_create();
	hashtbl_add(_hash, 0, &mm[0]);
	hashtbl_add(_hash, 1, &mm[1]);
	hashtbl_add(_hash, 2, &mm[2]);


	ms *_f = (ms*)hashtbl_find(_hash, 1);
	printf("%s\n", _f->attr.c_str());

	_f = (ms*)hashtbl_find(_hash, 10);
	printf("%p\n", _f);

	hashtbl_dump(_hash);
	hashtbl_del(_hash, 1);
	hashtbl_dump(_hash);
	hashtbl_free(_hash);

}
#endif

#ifdef __cplusplus
}
#endif


