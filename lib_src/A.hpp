#pragma once

#define DEBUG
class A {
public:
	A() = default;
	virtual ~A() = default;

	void op();

private:
	void _ordered_map();
};

