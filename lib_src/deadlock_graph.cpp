#include "shm-deadlock.hpp"
#include "deadlock_graph.hpp"
#include <glog/logging.h>
#include <string>
#include <unordered_map>
#include <queue>
#include <list>

#ifdef __cplusplus
extern "C" {
#endif
DeadlockGraph *gbl_dlock_graph = nullptr;
// reverse graph
DeadlockGraph *gbl_dlock_graph_r = nullptr;
struct ShmRWLock *gbl_dgraph_lck = nullptr;
static int _add_edge(DeadlockGraph &g, const std::string& obj_name,
					 TXNNodeId u, RSCNodeId v);
/**
 * Idea. reverse the common intuition
 * txn: u, resource: v
 * u->v, indicates resource v is holding by txn u
 * v->u, indicates txn u requiring resource v
 * Before txn(u) requires a lock(v) we should put an edge(v->u)
 * into the graph(if it's safe), once txn gets the lock, we need
 * first remove edge v->u, and add edge u->v indicating lock(v)
 * is now holding by txn(u).
 *
 * Safe:
 * To judge whether the stuation is safe, before adding edge v->u,
 * we need to check if there is a path from v to u already exists,
 * if it is, then not safe, if it's not, it's ok.
 *
 * All operations should be protected by gbl_dgraph_lck
 *
 * The change state should always be v->u, u->v, and delete
 * IE, acquire(add)->owned(flip)->release(remove)
 *
 * TXN is negative and resource is positive to distinguish
 * these two kind of Node in case of conflict.
 *
 * Procedure:
 * is_safe ? yes: add_edge->got lock->flip_edge->release lock->remove_edge
 * no: abort, rollback
 */

/**
 * add an edge from node u to node v
 * edge: u->v
 */
int dgraph_add_adge(RSCNodeId u, TXNNodeId v)
{
	assert(gbl_dlock_graph != nullptr);
	assert(gbl_dlock_graph_r != nullptr);
	auto &dl_graph = *gbl_dlock_graph;
	auto &dl_graph_r = *gbl_dlock_graph_r;

	// we never remove TXNNodeId from graph
    /*
	 *auto _u_count = dl_graph.count(u);
     */
	// here we also consider node _v, because we will flip the edge
	// to v->u later on, and it's also useful for finding the path
	// from u to v
    /*
	 *auto _v_count = dl_graph.count(v);
     */

	// header node does not exist, insert it
	// insert edge u->v
	_add_edge(dl_graph, std::to_string(u), u, v);
#if 0
	if (dl_graph.find(u) == dl_graph.end()) {
		auto adj_list = gbl_mem_seg->template
			construct<ADJList>(std::to_string(u).c_str())(gbl_mem_seg->get_segment_manager());
		auto ret = dl_graph.insert({u, *adj_list});
		assert(ret.second == true);
	}
#endif
	// insert edge v->u into graph_r
	_add_edge(dl_graph_r, std::to_string(u)+"r", v, u);
#if 0
	if (dl_graph_r.find(v) == dl_graph_r.end()) {
		auto adj_list = gbl_mem_seg->template
			construct<ADJList>((std::to_string(v)+"r").c_str())(gbl_mem_seg->get_segment_manager());
		auto ret = dl_graph_r.insert({v, *adj_list});
		assert(ret.second == true);
	}
#endif

    /*
	 *if (_v_count == 0) {
	 *    auto adj_list = gbl_mem_seg->template
	 *        construct<ADJList>(std::to_string(v).c_str())(gbl_mem_seg->get_segment_manager());
	 *    auto ret = dl_graph.insert({v, *adj_list});
	 *    assert(ret.second == true);
	 *}
     */
/*
 *    auto &adj_set = dl_graph.find(u)->second;
 *    // set v into u's adjacent list
 *    auto ret = adj_set.insert(v);
 *    assert(*ret.first == v || ret.second == true);
 *
 *    auto &adj_set_r = dl_graph_r.find(v)->second;
 *    ret = adj_set_r.insert(u);
 *    assert(*ret.first == u || ret.second == true);
 */

	return 0;
}

static void dump_paths(std::unordered_map<TXNNodeId, int> &dist);
/**
 * Find the shortest path from u to v in graph
 * gbl_dlock_graph, this function will find all
 * the pathes of node u to all the other nodes v;
 * but we only use the path u->v here.
 * For normal graph:
 * Time Complexity: O(|E| + |V|)
 * Space complexity: O(|V|)
 * For Dag:
 * We can do this in a linear approach
 */
static decltype(auto)
path_exists(TXNNodeId u, RSCNodeId v)
{
	auto &g = *gbl_dlock_graph;
	auto &g_r = *gbl_dlock_graph_r;
	auto _u = g.find(u);
	// source not exist
	if (_u == g.end() && g_r.find(u) == g_r.end()) {
		return 0;
	}
	// sink node not exist in the graph , return 0,
	// find it in the reverse graph, since if there
	// is no adj node in the original graph, there
	// would be no such node
	if (g_r.find(v) == g_r.end() && g.find(v) == g.end()) {
		return 0;
	}

	std::unordered_map<NodeId, int> dist;
	for (auto &node : g) {
		dist[node.first] = INT_MIN;
	}
	// add rsc nodes in g_r which does not
	// exist in g
	for (auto &node : g_r) {
		if (node.first > 0)
			dist[node.first] = INT_MIN;
	}
    /*
	 *LOG(INFO) << "-------";
	 *dump_dgraph();
     */

	dist[u] = 0;
	std::queue<NodeId> bfs_Q;
	bfs_Q.push(u);
	while (!bfs_Q.empty()) {
		auto n = bfs_Q.front();
		bfs_Q.pop();
		auto adj_set = g.find(n);
		if (adj_set != g.end()) {
			for (auto &x : adj_set->second) {
				if (dist[x] == INT_MIN) {
					bfs_Q.push(x);
					dist[x] = dist[n] + 1;
				}
			}
		}
	}
#ifdef DEADLOCK_DEBUG
	dump_paths(dist);
#endif
	LOG(INFO) << "dist[" << v << "]: " << dist[v];
	return dist[v] >= 0 ? 1 : 0;
}

int find_path_dag(NodeId u, NodeId v)
{
	return 0;
}

/**
 * To see whethe it leads a circle if we are to add
 * edge v->u into the graph gbl_dlock_graph.
 * Idea: decide if there is a path from v to u
 * in the existing graph, if it is, we should not add
 * the resource , if not, it's safe to add edge v->u
 * into the graph.
 * assert(u < 0 && v > 0)
 */
bool lead_to_circle(TXNNodeId u, RSCNodeId v)
{
	return path_exists(u, v);
}

/**
 * add edge u->v to graph g
 */
static int _add_edge(DeadlockGraph &g, const std::string& obj_name,
					 TXNNodeId u, RSCNodeId v)
{
	auto _u = g.find(u);
	// not exist, insert
	if (_u == g.end()) {
		auto adj_ret = find_shm_object<ADJList>(gbl_mem_seg, obj_name.c_str());
		// already exists
		auto adj_set = adj_ret.first;
		// not exists
		if (adj_ret.second == 0) {
			adj_set = gbl_mem_seg->template
				construct<ADJList>(obj_name.c_str())(gbl_mem_seg->get_segment_manager());
		}
		auto ret = g.insert({u, *adj_set});
		assert(ret.second == true);
	}
	// insert v to u
	_u = g.find(u);
	auto ret = _u->second.insert(v);
	assert(*ret.first == u || ret.second == true);

	return 0;
}

/**
 * flip an edge
 * u < 0 && v > 0
 */
int flip_edge(RSCNodeId u, TXNNodeId v)
{
	auto &g = *gbl_dlock_graph;
	auto &g_r = *gbl_dlock_graph_r;
	assert(g.find(u) != g.end());

	// handle g
	// first remove edge u->v
	auto _u = g.find(u);
	size_t cnt = _u->second.erase(v);
	assert(cnt == 1);
	if (u > 0) {
		if (_u->second.empty())
			g.erase(u);
	}

	// then insert edge v->u
	_add_edge(g, std::to_string(v), v, u);

#if 0
	auto _v = g.find(v);
	// not exist, insert
	if (_v == g.end()) {
		std::string obj_name = std::to_string(v);
		auto adj_ret = find_shm_object<ADJList>(gbl_mem_seg, obj_name.c_str());
		// already exists
		auto adj_set = adj_ret.first;
		// not exists
		if (adj_ret.second == 0) {
			adj_set = gbl_mem_seg->template
				construct<ADJList>(obj_name.c_str())(gbl_mem_seg->get_segment_manager());
		}
		auto ret = g.insert({v, *adj_set});
		assert(ret.second == true);
	}
	// insert u to v
	_v = g.find(v);
	auto ret = _v->second.insert(u);
	assert(*ret.first == v || ret.second == true);
#endif

	// handle g_r
	auto _v_r = g_r.find(v);
	cnt = _v_r->second.erase(u);
	assert(cnt == 1);
	if (v > 0) {
		if (_v_r->second.empty())
			g_r.erase(v);
	}

	_add_edge(g_r, std::to_string(v)+"r", u, v);
#if 0
	auto _u_r = g_r.find(u);
	// not exist, insert
	if (_u_r == g_r.end()) {
		std::string obj_name = std::to_string(u) + "r";
		auto adj_ret = find_shm_object<ADJList>(gbl_mem_seg, obj_name.c_str());
		// already exists
		auto adj_set = adj_ret.first;
		// not exists
		if (adj_ret.second == 0) {
			adj_set = gbl_mem_seg->template
				construct<ADJList>(obj_name.c_str())(gbl_mem_seg->get_segment_manager());
		}
		auto ret = g_r.insert({u, *adj_set});
		assert(ret.second == true);
	}
	_u_r = g_r.find(u);
	auto ret = _u_r->second.insert(v);
	LOG(INFO) << "u: " << u << ", " << *ret.first << ", " << ret.second;
	assert(*ret.first == u || ret.second == true);
#endif

	return 0;
}

bool has_edge(TXNNodeId u, RSCNodeId v)
{
	auto &g = *gbl_dlock_graph;
	auto _u = g.find(u);
	if (_u == g.end())
		return false;
	if (_u->second.find(v) == _u->second.end())
		return false;
	return true;
}


/**
 * remove edge from u to v, v is resource and u is txn
 */
int remove_edge(TXNNodeId u, RSCNodeId v)
{
	auto &g = *gbl_dlock_graph;
	auto &g_r = *gbl_dlock_graph_r;
	assert(g.find(u) != g.end());

	auto _u = g.find(u);
	auto ret = _u->second.erase(v);
	assert (ret == 1);
#if 1
	if (u > 0) {
		if (_u->second.empty()) {
			ret = g.erase(u);
			assert (ret == 1);
		}
	}
#endif
	auto _v_r = g_r.find(v);
	ret = _v_r->second.erase(u);
	assert(ret == 1);
	if (_v_r->second.empty()) {
		ret = g_r.erase(v);
		assert(ret == 1);
	}
	return ret;
}

/**
 * remove all edges held by txn u.
 */
int remove_edges(TXNNodeId u)
{
	auto &g = *gbl_dlock_graph;
	auto &g_r = *gbl_dlock_graph_r;
	auto &adj_set = g.find(u)->second;
	for (auto it = adj_set.begin(); it != adj_set.end(); ) {
		//LOG(INFO) << "*it: " << *it;
		auto _v_r = g_r.find(*it);
		assert(_v_r->first == *it);

		auto ret = _v_r->second.erase(u);
#ifdef DEADLOCK_DEBUG
		dump_dgraph();
		LOG(INFO) << "*it " << *it << ", u: " << u << ", ret: " << ret;
#endif
		// for debug
		if (ret != 1) {
			dump_dgraph();
			LOG(INFO) << "*it " << *it << ", u: " << u << ", ret: " << ret;
		}

		assert(ret == 1);
		if (_v_r->second.empty()) {
			auto _ret = g_r.erase(*it);
			assert(_ret == 1);
		}
		it = adj_set.erase(it);
	}
	return 0;
}

void _dump_dgraph(DeadlockGraph &graph)
{
	std::string s;
	s += "\n{\n";
	for (auto &x : graph) {
		s += '\t' + std::to_string(x.first) + " -> " + "{";
		auto prev_len = s.length();
		for (auto &y : x.second) {
			s += std::to_string(y) + ", ";
		}
		if (prev_len < s.length()) {
			s.pop_back();
			s.pop_back();
			s += ' ';
		}
		//LOG(INFO) << "===== " << s.length() << ": " << s << ": " << s[s.length()-2] << "#####";
		s += "};\n";
	}
	s += "},\n";
	LOG(INFO) << s;
}

void dump_dgraph()
{
	LOG(INFO) << __func__;
	//pthread_rwlock_rdlock(&gbl_dgraph_lck->rw_lock);
	_dump_dgraph(*gbl_dlock_graph);
	LOG(INFO) << "gbl_dlock_graph_r";
	_dump_dgraph(*gbl_dlock_graph_r);
	//pthread_rwlock_unlock(&gbl_dgraph_lck->rw_lock);
}

static void dump_paths(std::unordered_map<TXNNodeId, int> &dist) {
	std::string s;
	for (auto &x : dist)
		s += (std::to_string(x.first) + ": "
			+ std::to_string(x.second)) + ", ";
	LOG(INFO) << s;
}

#ifdef __cplusplus
}
#endif
