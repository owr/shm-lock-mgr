#ifndef __SHM_MAP_HPP__
#define __SHM_MAP_HPP__

#include "shm-obj.hpp"
#include <glog/logging.h>
/**
 * construct shared memory hash map
 */
static UODR_HashMap *create_shm_hashmap(const char *map_name)
{
	auto _seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);
	UODR_HashMap *hashmap = nullptr;
	auto pr = find_shm_object<UODR_HashMap>(_seg, map_name);

	using namespace std;
	// if not exists, then create
#ifdef PGLK_DEBUG
	LOG(INFO) << "tid: " << gettid() << ", pr.first: " << pr.first;
	LOG(INFO) << "tid: " << gettid() << ", map_name: " << map_name;
#endif
	if (!pr.first) {
		hashmap = _seg->construct<UODR_HashMap>(map_name)
			(3, boost::hash<uint32_t>(), std::equal_to<uint32_t>()
			 , _seg->get_allocator<ValueType>());
	} else {
#ifdef PGLK_DEBUG
		LOG(INFO) << "tid: " << gettid() << ", hashmap: " << map_name
			<< " existed, just return";
#endif
		hashmap = pr.first;
	}

	gbl_mem_seg = _seg;

	return hashmap;
}

/**
 * destroy shared memory hash map
 */
static void destroy_shm_hashmap(const char *map_name)
{
	/**
	 * skip the shm object destroy, destroy the whole shared memory
	 * directly, shm object destroy will take a long time because
	 * pointer cast back and forth in boost, ptr_offset
	 */
	destroy_shm_object<UODR_HashMap>(gbl_mem_seg, map_name);
}

#endif
