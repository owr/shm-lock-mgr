#ifndef __DEADLOCK_GRAPH_HPP__
#define __DEADLOCK_GRAPH_HPP__

#include "shm-deadlock.hpp"
#include <glog/logging.h>

#ifdef __cplusplus
extern "C" {
#endif

int dgraph_add_adge(RSCNodeId u, TXNNodeId v);
void _dump_dgraph(DeadlockGraph &graph);
void dump_dgraph();
bool lead_to_circle(TXNNodeId u, RSCNodeId v);
int flip_edge(RSCNodeId u, TXNNodeId v);
int remove_edge(TXNNodeId u, RSCNodeId v);
int remove_edges(TXNNodeId u);
bool has_edge(TXNNodeId u, RSCNodeId v);

#ifdef __cplusplus
}
#endif

#endif






