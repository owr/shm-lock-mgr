#ifndef _API_H_
#define _API_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * for api testing
 */
void op();

#include "hash_table.h"
#include "page_lock.h"


#ifdef __cplusplus
}
#endif


#endif
