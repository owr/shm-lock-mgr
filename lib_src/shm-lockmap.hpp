#ifndef __SHM_MAP_LOCKMAP_HPP__
#define __SHM_MAP_LOCKMAP_HPP__

#include "shm-obj.hpp"

/**
 * For holding transaction lock list
 * construct shared memory transaction lock list
 */
static TXNLockMap *create_shm_lockmap(const char *lockmap_name)
{
	auto _seg = init_shm_seg(HASH_SEG_NAME, HASH_SEG_MAX_SIZE);
	TXNLockMap *lockmap = nullptr;
	auto pr = find_shm_object<TXNLockMap>(_seg, lockmap_name);

	using namespace std;
	// if not exists, then create
#ifdef PGLK_DEBUG
	std::cout << "tid: " << gettid() << ", pr.first: " << pr.first << endl;
	std::cout << "tid: " << gettid() << ", lockmap_name: " << lockmap_name << endl;
#endif
	if (!pr.first) {
		lockmap = _seg->construct<TXNLockMap>(lockmap_name)
			(3, boost::hash<Pgno>(), std::equal_to<Pgno>()
			 , _seg->get_allocator<TXNLockMapMappedType>());
	} else {
#ifdef PGLK_DEBUG
		std::cout << "tid: " << gettid() << ", lockmap: " << lockmap_name
			<< " existed, just return" << std::endl;
#endif
		lockmap = pr.first;
	}

	//gbl_mem_seg = _seg;

	return lockmap;
}

/**
 * destroy shared memory hash map
 */
static void destroy_shm_lockmap(const char *lockmap_name)
{
	/**
	 * skip the shm object destroy, destroy the whole shared memory
	 * directly, shm object destroy will take a long time because
	 * pointer cast back and forth in boost, ptr_offset
	 */
	destroy_shm_object<TXNLockMap>(gbl_mem_seg, lockmap_name);
}

#endif
