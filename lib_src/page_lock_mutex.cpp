#include <pthread.h>
#include <atomic>
#include <vector>
#include <thread>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <stdint.h>
#include <sys/wait.h>
#include <glog/logging.h>
#include "shm-map.hpp"
#include "shm-lockmap.hpp"
#include "shm-deadlock.hpp"
#include "page_lock.h"
#include <mutex>
using namespace std;
using namespace boost::interprocess;

#ifdef __cplusplus
extern "C" {
#endif

struct ShmRWLock *gbl_seg_lck = nullptr;
void *gbl_lock_mgr = nullptr;
std::shared_ptr<managed_shared_memory> gbl_mem_seg;

static struct PageLockMtx *_mgr_search_page(uint32_t pgno);
static int _mgr_insert_page(uint32_t pgno);
int64_t gbl_txn_id = 0;

void lock_mgr_init()
{
	//std::ios::sync_with_stdio(false);
	gbl_seg_lck = init_shared_rwlock(SHARED_RWLOCK);
	int32_t ref_cnt = gbl_seg_lck->ref_count.fetch_add(1);
	gbl_dgraph_lck = init_shared_rwlock(SHARED_DGRAPH_LOCK);
	ref_cnt = gbl_dgraph_lck->ref_count.fetch_add(1);
#ifdef PGLK_DEBUG
    /*
	 *LOG(INFO) << "tid: " << gettid() << ", ref_cnt: " << ref_cnt;
     */
#endif
	pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
	gbl_lock_mgr = create_shm_hashmap(HASH_MAP_NAME);
	// init txn lock mapper
	gbl_txn_lock_map = create_shm_lockmap(SHARED_TXN_LOCKMAP);
	gbl_dlock_graph = create_shm_dgraph(SHARED_DGRAPH);
	gbl_dlock_graph_r = create_shm_dgraph(SHARED_DGRAPH_R);
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
}

void lock_mgr_destroy()
{
#if 0
	int32_t ref_cnt = gbl_seg_lck->ref_count.fetch_sub(1);
	LOG(INFO) << "tid: " << gettid() << ", ref_cnt: " << ref_cnt;
	if (ref_cnt == 0) {
#endif
#if 1
		pthread_rwlock_destroy(&gbl_seg_lck->rw_lock);
		pthread_rwlock_destroy(&gbl_dgraph_lck->rw_lock);
		// destroy SHARED_RWLOCK in shared memory segment
		destroy_shared_rwlock(SHARED_RWLOCK);
		destroy_shared_rwlock(SHARED_DGRAPH_LOCK);
		// destroy shared hashmap in shared memory segment
		destroy_shm_hashmap(HASH_MAP_NAME);
		// destroy shared shared lock list
		destroy_shm_lockmap(SHARED_TXN_LOCKMAP);
		// destroy shared shared deadlock graph
		destroy_shm_dgraph(SHARED_DGRAPH);
		destroy_shm_dgraph(SHARED_DGRAPH_R);
		// destroy the shared segment itself
		destroy_seg();
#endif
#if 0
	}
#endif
}

/**
 * return the lock status
 * 0, succed
 * 1, not exist
 * -1 failed
 */
int mgr_enter_mutex(uint32_t pgno)
{
#ifdef MTX_NOLOCK
	return 0;
#endif

#ifdef PGLK_DEBUG1
	LOG(INFO) << __func__ << ", tid: " << gettid() << ", try to lock pgno: " << pgno;
#endif
	struct PageLockMtx *_pglck = nullptr;

	pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
	_pglck = _mgr_search_page(pgno);

#ifdef PGLK_DEBUG
    /*
	 *LOG(INFO) << "tid: " << gettid() << ", before _pglock: " << _pglck;
     */
#endif
	if (_pglck == nullptr) {
#ifdef PGLK_DEBUG
		LOG(INFO) << "tid: " << gettid() << ", rpage insert pgno: " << pgno;
#endif
		_mgr_insert_page(pgno);
	}
	_pglck = _mgr_search_page(pgno);
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	assert(_pglck != nullptr);
	auto ret = pthread_mutex_lock(&_pglck->pg_mutex);

	_pglck->stat = RD_LOCK;
	// increase refcount
	pthread_mutex_lock(&_pglck->ref_count_mtx);
	++_pglck->ref_count;
#ifdef PGLK_DEBUG
		LOG(INFO) << "pgno: " << pgno << ", ref_count: " << _pglck->ref_count;
#endif
	pthread_mutex_unlock(&_pglck->ref_count_mtx);

#ifdef PGLK_DEBUG1
	LOG(INFO) << "tid: " << gettid() << ", got lock pgno: " << pgno;
#endif
	return ret;
}

/***
 * try 5 times, every l nanoseconds
 */
int mgr_mutex_keep_trying(uint32_t pgno, uint64_t l)
{
	int ret;
	int cnt = 5;
	struct timespec req = {.tv_sec = 0 };
	req.tv_nsec = l;

	while (cnt-- && ((ret = mgr_try_enter_mutex(pgno)) != 0)) {
#ifdef PGLK_DEBUG1
		LOG(INFO) << "ret: " << ret <<
			", try locking " << 5 - cnt << "st time."
			<< " pgno: " << pgno;
#endif
		nanosleep(&req, NULL);
	}
#ifdef PGLK_DEBUG1
	LOG(INFO) << "ret: " << ret <<
		", try locking " << 5 - cnt << "st time."
		<< " pgno: " << pgno;
#endif
	return ret;
}

int mgr_try_enter_mutex(uint32_t pgno)
{
#ifdef MTX_NOLOCK
	return 0;
#endif

#ifdef PGLK_DEBUG1
	LOG(INFO) << __func__ << ", tid: " << gettid() << ", try to lock pgno: " << pgno;
#endif
	struct PageLockMtx *_pglck = nullptr;

	pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
	_pglck = _mgr_search_page(pgno);

#ifdef PGLK_DEBUG
    /*
	 *LOG(INFO) << "tid: " << gettid() << ", before _pglock: " << _pglck;
     */
#endif
	if (_pglck == nullptr) {
#ifdef PGLK_DEBUG
		LOG(INFO) << "tid: " << gettid() << ", rpage insert pgno: " << pgno;
#endif
		_mgr_insert_page(pgno);
	}
	_pglck = _mgr_search_page(pgno);
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	assert(_pglck != nullptr);
	int ret = pthread_mutex_trylock(&_pglck->pg_mutex);

	_pglck->stat = RD_LOCK;
	// increase refcount
	pthread_mutex_lock(&_pglck->ref_count_mtx);
	if (ret == 0)
		++_pglck->ref_count;
#ifdef PGLK_DEBUG
		LOG(INFO) << "pgno: " << pgno << ", ref_count: " << _pglck->ref_count;
#endif
	pthread_mutex_unlock(&_pglck->ref_count_mtx);

#ifdef PGLK_DEBUG1
	LOG(INFO) << "tid: " << gettid() << ", got lock pgno: " << pgno;
#endif
	return ret;
}

int mgr_leave_mutex(uint32_t pgno)
{
#ifdef MTX_NOLOCK
	return 0;
#endif

#ifdef PGLK_DEBUG
	LOG(INFO) << "pgno: " << pgno << "tid: " << gettid() << "start unlock";
#endif
	struct PageLockMtx *_pglck;

	//pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	_pglck = _mgr_search_page(pgno);
	//pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	assert(_pglck != nullptr);
#ifdef PGLK_DEBUG
	LOG(INFO) << "pgno: " << pgno << "unlocked, " << "tid: " << gettid();
#endif
	auto ret = pthread_mutex_unlock(&_pglck->pg_mutex);

	_pglck->stat = NO_LOCK;
	// decrease refcount
	pthread_mutex_lock(&_pglck->ref_count_mtx);
	--_pglck->ref_count;
#ifdef PGLK_DEBUG
		LOG(INFO) << "pgno: " << pgno << ", leave, ref_count: " << _pglck->ref_count;
#endif
	pthread_mutex_unlock(&_pglck->ref_count_mtx);

	return ret;
}

int mgr_leave_mutex_all(uint32_t pgno)
{

#ifdef PGLK_DEBUG
	LOG(INFO) << "tid: " << gettid() << ", start unlock pgno: " << pgno;
#endif
	struct PageLockMtx *_pglck;

	//pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	_pglck = _mgr_search_page(pgno);
	//pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	assert(_pglck != nullptr);
#ifdef PGLK_DEBUG
	LOG(INFO) << "tid: " << gettid() << ", unlocked pgno: " << pgno;
#endif

	int ret = 0;
	// decrease refcount
	pthread_mutex_lock(&_pglck->ref_count_mtx);
	while (_pglck->ref_count) {
		ret = pthread_mutex_unlock(&_pglck->pg_mutex);
#ifdef PGLK_DEBUG
		LOG(INFO) << __func__ << ", ret: " << ret;
		LOG(INFO) << "pgno: " << pgno << ", leaveall, ref_count: " << _pglck->ref_count;
#endif
		assert(ret == 0);
		--_pglck->ref_count;
	}
#ifdef PGLK_DEBUG
	assert(_pglck->ref_count == 0);
	LOG(INFO) << "pgno: " << pgno << ", leaveall, ref_count: " << _pglck->ref_count;
#endif
	pthread_mutex_unlock(&_pglck->ref_count_mtx);
	return ret;
}

static int _mgr_insert_page(uint32_t pgno)
{
	PageLockMtx _lock;
	memset(&_lock, 0, sizeof(PageLockMtx));
	_lock.pageno = pgno;

	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	/**
	 * set process shared, important
	 */
	pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
	// recursive
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE_NP);


	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	if (exists)
		goto ret;
	/**
	 * insert the lock
	 */
	// init page lock
	obj.insert(ValueType(pgno, _lock));
	pthread_mutex_init(&obj[pgno].pg_mutex, &mutex_attr);
	// init protecter
	pthread_mutex_init(&obj[pgno].ref_count_mtx, &mutex_attr);
ret:
	return 0;
}

int mgr_insert_page(uint32_t pgno)
{
	PageLockMtx _lock;
	memset(&_lock, 0, sizeof(PageLockMtx));
	_lock.pageno = pgno;
	_lock.stat = NO_LOCK;

	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	/**
	 * set process shared, important
	 */
	pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
	// recursive
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE_NP);

	/**
	 * lock the lock manager to insert a page, manager write
	 */
	pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
	/**
	 * check the page lock if already exists, if so, just return
	 * this could happen when there are two writers trying to insert
	 * the same page
	 */
	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	if (exists)
		goto ret;
	/**
	 * insert the lock
	 */
	obj.insert(ValueType(pgno, _lock));
	pthread_mutex_init(&obj[pgno].pg_mutex, &mutex_attr);
	pthread_mutex_init(&obj[pgno].ref_count_mtx, &mutex_attr);
ret:
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);

	return 0;
}

static struct PageLockMtx *_mgr_search_page(uint32_t pgno)
{
	struct PageLockMtx *_pglck = nullptr;

	/**
	 * lock the lock manager to search a page, manager read
	 */
	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
#ifdef PGLK_DEBUG
	auto res = exists ? "exists" : "not exists";
	//LOG(INFO) << "tid: " << gettid() << ", page lock " << res;
#endif
	if (exists)
		_pglck = &obj[pgno];

	return _pglck;
}

struct PageLockMtx *mgr_search_page(uint32_t pgno)
{
	struct PageLockMtx *_pglck = nullptr;

	/**
	 * lock the lock manager to search a page, manager read
	 */
	pthread_rwlock_rdlock(&gbl_seg_lck->rw_lock);
	auto &obj = *static_cast<UODR_HashMap*>(gbl_lock_mgr);
	auto exists = obj.count(pgno);
	if (exists)
		_pglck = &obj[pgno];

	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
	return _pglck;
}

/**
 * for test
 */
void dump_mrg()
{
	pthread_rwlock_wrlock(&gbl_seg_lck->rw_lock);
	for (auto &x : *static_cast<UODR_HashMap*>(gbl_lock_mgr))
#ifdef PGLK_DEBUG
		LOG(INFO) << "pgno: " << x.first << ", "
			<< x.second.pageno << ", lock: "
			<< &x.second.pg_mutex << std::endl;
#endif
	pthread_rwlock_unlock(&gbl_seg_lck->rw_lock);
}

#ifdef __cplusplus
}
#endif
